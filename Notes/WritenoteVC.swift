    //
//  WritenoteVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 23/11/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.

    
import UIKit
import AVFoundation
import MobileCoreServices
import FirebaseDatabase
import Firebase

class WritenoteVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,selectlabelnamedelegate,AVAudioPlayerDelegate,selectdate,UICollectionViewDelegateFlowLayout
{
    var uid = String()
    var cx = CGFloat(10)
    var tag = Int(0)
    
    var myRootRef: FIRDatabaseReference!
    var permissionid = String()
    @IBOutlet weak var btnplay: UIButton!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    
    @IBOutlet weak var lbldate_time: UILabel!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet  var viewmain: UIView!
    @IBOutlet var txttitle: UITextField!
    @IBOutlet var txtdescription: UITextView!
    @IBOutlet weak var btnimage: UIButton!
    @IBOutlet weak var imagecollection: UICollectionView!
    @IBOutlet var heightConstraint: NSLayoutConstraint!
    @IBOutlet var Heighttbl: NSLayoutConstraint!
    @IBOutlet weak var tblaudio: UITableView!
    var topConstraint : NSLayoutConstraint!
    @IBOutlet weak var btnsetting: UIButton!
    
    @IBOutlet weak var actionsheet: UIView!
    @IBOutlet var viewcolor: UIView!
    var arrItems = NSMutableArray()
    var fileid = String()
    var urlString = String()
    var arrimages = NSMutableArray()
    var cellheight : Float = 0.0
    var addimages = NSMutableArray()
    var dictnote = NSDictionary()
    var arraudio = NSMutableArray()
    var color : String = "FFFFFF"
    var red : Float = 255.0
    var green : Float = 255.0
    var blue : Float = 255.0
    
    //MARK:- recording view--------------------------------

 
    @IBOutlet weak var viewplayer: UIView!
     var audioPlayer:AVAudioPlayer  = AVAudioPlayer()
    
    @IBOutlet weak var progressView: UIProgressView!
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if dictnote.count != 0
        {
            txttitle.text = dictnote["Title"]as? String
            txtdescription.text = dictnote["SubTitle"]as? String
            let points : String = dictnote["label"]as! String

            color = dictnote["hexa"]as! String
            viewcolor.backgroundColor = hexStringToUIColor(hex: color)
            let fullNameArr = points.components(separatedBy: ",")
            for i in 0..<fullNameArr.count
            {
                if fullNameArr[i] != ""
                {
                   arrItems.add(fullNameArr[i])
                }
                
            }
            
            let arrimg : NSArray = DBOperation.selectData(String(format: "select * from Note_image where noite_uid='%@'",dictnote["note_uid"]as! String))
            
            for i in 0..<arrimg.count
            {
                let temp = (arrimg[i] as? NSDictionary)?.object(forKey: "image_name")as? String
                addimages.add(temp!)
            }
            imagecollection.reloadData()
            
            self.setframe()
            let arraudio : NSArray = DBOperation.selectData(String(format: "select * from Note_audio where noite_uid='%@'",dictnote["note_uid"]as! String))
            
            if arraudio.count != 0
            {
                viewplayer.isHidden = false
                let dict : NSDictionary = arraudio[0]as! NSDictionary
                urlString = dict["audio_name"]as! String
                arraudio.adding(urlString)
            }
        }else if urlString != ""
        {
            arraudio.adding(urlString)
            viewplayer.isHidden = false
        }
        else
        {
            txtdescription.text = "Write note"
           
        }
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd:MM:yyyy hh:mm"
        let keystring = formatter.string(from: now)
        lbldate_time.text = keystring.appending(":00")
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    
    func getdate(arr: String)
    {
        //lbldate_time.text = arr
    }

    func updateAudioProgressView()
    {
        if audioPlayer.isPlaying
        {
            progress.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
      self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    @IBAction func btnplay(_ sender: Any)
    {
        let url = NSURL(fileURLWithPath: urlString)
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
        
        do {
            try  audioPlayer =  AVAudioPlayer(contentsOf: NSURL(string:soundFileURL) as! URL)
            audioPlayer.play()
            Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateAudioProgressView), userInfo: nil, repeats: true)
            progress.setProgress(Float(audioPlayer.currentTime/audioPlayer.duration), animated: false)
            let time : Float = Float(audioPlayer.duration)
            lbltime.text = time.description
        }
        catch
        {
             print("audioPlayer error \(error.localizedDescription)")
        }
    }
    
    func removefile(itemName:String, fileExtension: String) {
        
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(itemName).\(fileExtension)"
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }

    func labelname(arr : NSMutableArray)
    {
        arrItems = arr
        if arrItems.count == 0
        {
            scroll.isHidden = true
        }else
        {
            scroll.isHidden = false
        }

        self.setframe()
    }

    @IBAction func btnleftprassed(_ sender: Any) {
        
        savedata()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func savedata() -> Void
    {
        self.calculateheight()
        for i in 0..<addimages.count
        {
            let url = NSURL(fileURLWithPath: addimages[i]as! String)
            let soundFileURL = url.lastPathComponent
            arrimages.add(soundFileURL!)
        }
        
        
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        
        let autono : NSArray = DBOperation.selectData(String(format:"select * from Notes"))
        let Randomnumber : Int = Int(arc4random_uniform(UInt32(10000)))
        let number :Int = Int (arc4random() % 16)
        let abc : Int = autono.count + 1;
        let final : String = "note" + abc.description + Randomnumber.description + number.description
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let keystring = formatter.string(from: now)
        let key = keystring + autono.count.description
        
        var name = String()
        
        if urlString == ""
        {
            name = " "
        }
        else
        {
            
            let url = NSURL(fileURLWithPath: urlString)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
            name  =  url.lastPathComponent!
            let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
            let localFile = URL(string: documentsPath)!
            let storageRef = FIRStorage.storage().reference()
            let mountainsRef = storageRef.child("audio/"+name)
            let metadata = FIRStorageMetadata()
            metadata.contentType = "audio/mpeg"
            mountainsRef.put(data as Data, metadata: metadata)
            mountainsRef.putFile(localFile, metadata: metadata)
            if dictnote.count == 0
            {
                DBOperation.executeSQL(String(format: "Insert into Note_audio('noite_uid','audio_name','key') Values('%@','%@','%@')",final,urlString,key))
            }
        }
        
        if txttitle.text != "" && txtdescription.text != ""
        {
            myRootRef = FIRDatabase.database().reference()
            let dict : NSDictionary = ["checklist":"","flag":""]
            let arr : NSArray = [dict]
            var image = NSArray()
            var label = NSArray()
            if arrimages.count == 0
            {
                image = [""]
            }
            else
            {
                image = arrimages
            }
            
            if arrItems.count == 0
            {
                label = [""]
            }
            else
            {
                label = arrItems
            }
            if dictnote.count == 0
            {
                permissionid = key
                self.myRootRef.child("Note").child(key).child("data").setValue([
                    "Title": txttitle.text!,
                    "description": txtdescription.text!,
                    "color" : color,
                    "display" : "true",
                    "time":lbldate_time.text!,
                    "cellheight":cellheight.description,
                    "note_uid":final,
                    "Images" : image,"Labels":label,"audio" : name.description,"checklist" : arr])
                
                let arruid : NSArray = [uid]
                self.myRootRef.child("Note").child(key).child("accessuid").setValue([
                    "userid": arruid])

                DBOperation.executeSQL(String(format: "Insert into Notes('Title','SubTitle','note_uid','label','key','hexa','date','cellheight','display') Values('%@','%@','%@','%@','%@','%@','%@','%f','%@')",txttitle.text!,txtdescription.text!,final,arrItems.componentsJoined(by: ","),key,color,lbldate_time.text!,cellheight,"true"))
                self.saveimage(noteid :final)
                
            }else{
            
                permissionid = dictnote["key"]as! String
                self.myRootRef.child("Note").child(dictnote["key"]as! String).child("data").setValue([
                    "Title": txttitle.text!,
                    "description": txtdescription.text!,
                    "note_uid":dictnote["note_uid"] as! String,
                    "color" : color,
                    "display" : "true",
                    "time":lbldate_time.text!,
                    "cellheight":cellheight.description,
                    "Images" : image,
                    "Labels" : label,"audio" : name.description ,"checklist" : arr])
                
                let arruid : NSArray = [uid]
                self.myRootRef.child("Note").child(dictnote["key"]as! String).child("accessuid").setValue([
                    "userid": arruid])

                self.saveimage(noteid :dictnote["note_uid"] as! String)
                DBOperation.executeSQL(String(format: "update Notes set Title='%@',SubTitle='%@',label='%@',hexa='%@',date='%@',cellheight='%f','display'='%@' where note_uid='%@'",txttitle.text!,txtdescription.text!,arrItems.componentsJoined(by: ","),color,lbldate_time.text!,cellheight,"true",dictnote["note_uid"] as! String!))
            }
            
        }
    }
    
    func calculateheight () ->Void
    {
        if addimages.count != 0
        {
            cellheight = 85.0
        }
        cellheight = cellheight + 17.0 + Float(txtdescription.frame.size.height)
        if cellheight >= 180.0
        {
            cellheight = 180.0
        }
    }
    @IBAction func btnshareprassed(_ sender: Any) {
       
        savedata()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "searchuserVC") as! searchuserVC
        vc.keyid = permissionid
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    
    }
    
    func createpdffromview(aview : UIView , filename : String)
    {
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, aview.frame, nil)
        UIGraphicsBeginPDFPage()
        let pdfContext = UIGraphicsGetCurrentContext();
        aview.layer .render(in: pdfContext!)
        UIGraphicsEndPDFContext()
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent(filename)
        
        if !FileManager.default.fileExists(atPath: fileURL.path)
        {
            pdfData.write(to: URL(fileURLWithPath: fileURL.path), atomically: true)
        }
        let url : URL = NSURL .fileURL(withPath: fileURL.path)
        if tag == 5
        {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "searchuserVC") as! searchuserVC
          //  vc.pdffile = url.absoluteString
            self.view.addSubview(vc.view)
            self.addChildViewController(vc)
            vc.didMove(toParentViewController: self)
            
        }
        else
        {
            let activityItem: [AnyObject] = [url as AnyObject]
            let avc = UIActivityViewController(activityItems: activityItem as [AnyObject], applicationActivities: nil)
            let excludedActivities : NSArray = []/*UIActivityTypePostToTwitter, UIActivityTypePostToFacebook,
             UIActivityTypePostToWeibo,
             UIActivityTypeMessage, UIActivityTypeMail,
             UIActivityTypePrint, UIActivityTypeCopyToPasteboard,
             UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll,
             UIActivityTypeAddToReadingList, UIActivityTypePostToFlickr,
             UIActivityTypePostToVimeo, UIActivityTypePostToTencentWeibo];*/
            
            avc.excludedActivityTypes = excludedActivities as? [UIActivityType]
            if UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiom.pad
            {
                avc.popoverPresentationController?.sourceView = self.view
                self.present(avc, animated: true, completion: nil)
            }
            else
            {
                self.present(avc, animated: true, completion: nil)
            }
        }
    }
    
    //MARK:- Button touch up methods  -------------------------------
    
    
    
    
    
    func saveimage(noteid : String)
     {
        if dictnote.count != 0
        {
            DBOperation.executeSQL(String(format: "delete from Note_image where noite_uid='%@'",noteid))
            for i in 0..<addimages.count
            {
                let url = NSURL(fileURLWithPath: addimages[i]as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
                let name  =  url.lastPathComponent!
                let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
                let localFile = URL(string: documentsPath)!
                let storageRef = FIRStorage.storage().reference()
                let mountainsRef = storageRef.child("imags/"+name)
                
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                mountainsRef.put(data as Data, metadata: metadata)
                mountainsRef.putFile(localFile, metadata: metadata)
                
                DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name') Values('%@','%@')",noteid,(addimages[i]as? String)!))
            }
 
        }else{
            
            for i in 0..<addimages.count
            {
                let url = NSURL(fileURLWithPath: addimages[i]as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
                let name  =  url.lastPathComponent!
                let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
                let localFile = URL(string: documentsPath)!
                let storageRef = FIRStorage.storage().reference()
                let mountainsRef = storageRef.child("imags/"+name)
                
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                mountainsRef.put(data as Data, metadata: metadata)
                mountainsRef.putFile(localFile, metadata: metadata)
                
                DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name') Values('%@','%@')",noteid,(addimages[i]as? String)!))
            }
        }
    }

    @IBAction func btncancel(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            actionsheet.isHidden = true
            if self.dictnote.count != 0
            {
                let defaults = UserDefaults.standard
                let uid = defaults.value(forKey:"uid")as? String
                let ref = FIRDatabase.database().reference()
                
                let arraddimages : NSArray = DBOperation.selectData(String(format: "select * from Note_image where noite_uid='%@'",dictnote["note_uid"]as! String))
                for i in 0..<arraddimages.count
                {
                    let dicttemp : NSDictionary = arraddimages[i]as! NSDictionary
                    let url = NSURL(fileURLWithPath: dicttemp["image_name"]as! String)
                    let soundFileURL = url.lastPathComponent
                    print(soundFileURL!)
                    
                    let storageRef = FIRStorage.storage().reference()
                    let mountainsRef = storageRef.child("imags/" + soundFileURL!)
                    mountainsRef.delete { error in
                        if error != nil {
                                print("error")
                        } else {
                            print("deleted")
                        }
                    }
                }
                
                if urlString != ""
                {
                    let url = NSURL(fileURLWithPath: urlString)
                    let soundFileURL = url.lastPathComponent
                    print(soundFileURL!)
                    
                    let storageRef = FIRStorage.storage().reference()
                    let mountainsRef = storageRef.child("audio/" + soundFileURL!)
                    mountainsRef.delete { error in
                        if error != nil {
                            print("error")
                        } else {
                            print("deleted")
                        }
                    }

                }
                
                ref.child("Note").child(self.dictnote["key"]as! String).child("data").removeValue()
                DBOperation.executeSQL(String(format: "delete from Notes where note_uid = '%@'",self.dictnote["note_uid"] as! String))
                DBOperation.executeSQL(String(format: "delete from Note_image where noite_uid = '%@'",self.dictnote["note_uid"] as! String))
            }
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
            self.navigationController?.pushViewController(viewcontroller, animated: true)

        }
        if sender.tag == 2
        {
            actionsheet.isHidden = true
            if self.txttitle.text != "" || self.txtdescription.text != ""
            {
                tag = 6
                let now = NSDate()
                let formatter = DateFormatter()
                formatter.dateFormat = "ddMMyyHHmmss"
                formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
                let datestring = formatter.string(from: now as Date)
                let pdfname = self.txttitle.text! + datestring + ".pdf"
                self.createpdffromview(aview: self.viewmain, filename: pdfname)
            }
        }
        
        if sender.tag == 3
        {
            actionsheet.isHidden = true
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let  controller = mainStoryboard.instantiateViewController(withIdentifier: "LabelVC") as! LabelVC
            controller.note_uid = self.uid
            controller.arrayItem = self.arrItems
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    @IBAction func btndatetimeprassed(_ sender: Any)
    {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Date_timeVC") as! Date_timeVC
        vc.delegate = self
        vc.dictdata = dictnote
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    @IBAction func btnsettingPrassed(_ sender: Any) {
        
        if btnsetting.isSelected == true
        {
             btnsetting.isSelected = false
             actionsheet.isHidden = true
        }else
        {
            btnsetting.isSelected = true
             actionsheet.isHidden = false
        }
     
    
    }
    
    @IBAction func btnimageprassed(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        
        let CameraRoll = UIAlertAction(title: "Camera Roll", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
            
        })
      
        
        let Library = UIAlertAction(title: "Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
        })
        
        let Camera = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                imagePickerController.allowsEditing = false
                self.present(imagePickerController, animated: false, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Error!!", message: "Device Have No Camera", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(CameraRoll)
        optionMenu.addAction(Library)
        optionMenu.addAction(Camera)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
    }
    
    @IBAction func btncolorprassed(_ sender: UIButton)
    {
        actionsheet.isHidden = true
        if sender.tag == 1
        {
            color = "FFCBA4"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FFCBA4")
        }
        else if sender.tag == 2
        {
            color = "36DDB6"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "36DDB6")

        }
        else if sender.tag == 3
        {
            color = "FF0060"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FF0060")

        }
        else if sender.tag == 4
        {
            color = "FF9A0F"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FF9A0F")

        }
        else if sender.tag == 5
        {
            color = "7FE0FF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "7FE0FF")

        }
        else if sender.tag == 6
        {
            color = "C387FF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "C387FF")

        }
        else if sender.tag == 7
        {
            color = "FFFFFF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FFFFFF")
            
        }
    }
    
   
    //MARK:- imagepicker method------------------------------
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        let autono : NSArray = DBOperation.selectData(String(format:"select * from Notes"))
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let imagePath = formatter.string(from: now)
        let name = imagePath + autono.count.description + ".jpg"
        
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent(name)
        let urlString: String = fileURL.path
        
        if !FileManager.default.fileExists(atPath: fileURL.path)
        {
            if (try? UIImageJPEGRepresentation(image, 0.1)!.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])) != nil
            {
                print("file saved")
            }
            else
            {
                print("error saving file")
            }
        }
        addimages.add(urlString)
        imagecollection.reloadData()
    }
    
    //MARK:- collection view methods-----------------------------
   
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
        let height = imagecollection.frame.size.width / 3.0
        if addimages.count == 1 || addimages.count == 2 || addimages.count == 3
        {
            let height1 : CGFloat =  height * CGFloat(1)
            heightConstraint.constant = height1
            return addimages.count
        }
        else{
           
            var number = Int()
            for i in 0..<addimages.count
            {
                if i%3 == 0
                {
                    number = number + 1
                }
            }
            let height1 : CGFloat =  height * CGFloat(number)
            heightConstraint.constant = height1
            return addimages.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
    let cell = imagecollection.dequeueReusableCell(withReuseIdentifier: "Collectimage",for: indexPath) as! Collectimage
        cell.backgroundColor = UIColor.black
        if addimages.count != 0
        {
            let dicttemp = addimages[indexPath.row] as! String
            if dicttemp != ""
            {
               NSUtil.getImageFor(cell.imgnote, fromURL: dicttemp, alterText: nil)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
     
         let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 1
        let width : CGFloat = (imagecollection.bounds.width  - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
     
        return CGSize.init(width: width, height: imagecollection.frame.size.width / 3.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1.0;
    }
    //MARK:- textview delegate methods--------------------------------------
    
    public func textViewShouldBeginEditing(_ textView: UITextView) -> Bool
    {
        if dictnote.count != 0 || arrItems.count != 0
        {
            
        }
        else
        {
            txtdescription.text = ""

        }
        return true
    }
    
   //MARK:- userdefine mehods----------------------------------------
    
    func setframe() -> Void
    {
        let subViews = scroll.subviews
        for subview in subViews
        {
            subview.removeFromSuperview()
        }
        tag = 0
        var Xpoint : CGFloat = 10.0
        var Ypoint : CGFloat = 10.0
        
        let maxSize : CGSize = CGSize(width: self.view.frame.size.width - 30, height: 9999)

        for i in 0 ..< arrItems.count
        {
             let strItem = arrItems[i] as? String
            let btnlabel = UILabel()
            let titlesize : CGSize =  NSUtil .dynamicSize(fromText: strItem, with: UIFont.systemFont(ofSize: 12.0), withMaxSize: maxSize)
            if (Xpoint + titlesize.width + 15 < self.view.frame.size.width-20)
            {
              btnlabel .frame = CGRect(x: Xpoint, y: Ypoint, width: titlesize.width + 25 , height: titlesize.height + 10)
                 Xpoint = Xpoint + titlesize.width + 30
            }else{
                
                Xpoint = 10;
                Ypoint = Ypoint+titlesize.height+15;
                btnlabel .frame = CGRect(x: Xpoint, y: Ypoint, width: titlesize.width + 25 , height: titlesize.height+10)
                Xpoint = Xpoint + titlesize.width + 30
            }
            btnlabel.tag = tag
            btnlabel.text = strItem
            btnlabel .textColor = UIColor .black
            scroll .addSubview(btnlabel)
        }
    }
}

