//
//  searchuserVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 20/02/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import MessageUI
import Foundation

class searchuserVC: UIViewController,UITableViewDataSource,UITableViewDelegate,MFMailComposeViewControllerDelegate {

    var myRootRef: FIRDatabaseReference!
    @IBOutlet weak var tblusers: UITableView!
    @IBOutlet weak var txtsearch: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var arruser = NSArray()
    var arradduser = NSMutableArray()
    var dictuser = NSDictionary()
    var keyid = String()
    var dict = NSDictionary()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        spinner.startAnimating()
        self.view.isUserInteractionEnabled = false
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child("USERS").observeSingleEvent(of: .value, with:
            { (snapshot) in
                self.dictuser  = (snapshot.value as? NSDictionary)!
                self.arruser = self.dictuser.allKeys as NSArray
                self.spinner.stopAnimating()
                self.view.isUserInteractionEnabled = true
         })
        { (error) in
            print(error.localizedDescription)
        }
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btnsearchprassed(_ sender: Any)
    {
        for i in 0 ..<  arruser.count
        {
            let key = arruser[i] as! String
            let arrtemp : NSMutableDictionary = dictuser[key] as! NSMutableDictionary
            if arrtemp["User"]as? String == txtsearch.text
            {
               arradduser.add(arrtemp)
            }
            
        }
        tblusers.reloadData()
    }
    
    @IBAction func btnbackprassed(_ sender: Any)
    {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        self.navigationController?.pushViewController(controller, animated: true)
    }

    //MARK:- tableview delegate methods---------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arradduser.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:Labelcell = self.tblusers.dequeueReusableCell(withIdentifier: "Labelcell") as! Labelcell
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        let dict : NSDictionary = arradduser[indexPath.row] as! NSDictionary

        cell.lblname.text = dict["User"]as! String
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let dicttemp = arradduser[indexPath.row]as! NSDictionary
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let keystring = formatter.string(from: now)
        let defaults = UserDefaults.standard
        let uid = dicttemp["userid"]as! String
        myRootRef = FIRDatabase.database().reference()
        let notification = "Share note by ".appending(defaults.value(forKey:"name")as! String)
       let Randomnumber : Int = Int(arc4random_uniform(UInt32(10000)))
        myRootRef.child(uid).child("Notification").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                 self.myRootRef.child(uid).child("Notification").child(Randomnumber.description).setValue(["notification":notification,"key":self.keyid]);
                    self.myRootRef.child("Note").child(self.keyid).child("accessuid").observeSingleEvent(of: .value, with:
                    { (snapshot) in
                        let data  = snapshot.value as? NSDictionary
                        if data?.count == 0 || data == nil
                        {
                            let access : NSArray = [uid]
                            self.myRootRef.child("Note").child(self.keyid).child("accessuid").setValue([
                                "userid": access])

                        }
                        else
                        {
                            let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                            let access : NSMutableArray = data["userid"]as! NSMutableArray
                            access.add(uid)
                            self.myRootRef.child("Note").child(self.keyid).child("accessuid").setValue([
                                "userid": access])
                            
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                            self.navigationController?.pushViewController(controller, animated: true)
                            
                            self.view.removeFromSuperview()
                            self.removeFromParentViewController()
                        }
                    })
                }
                else
                {
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                self.myRootRef.child(uid).child("Notification").child(Randomnumber.description).setValue(["notification":notification,"key":self.keyid]);
                    self.myRootRef.child("Note").child(self.keyid).child("accessuid").observeSingleEvent(of: .value, with:
                        { (snapshot) in
                            let data  = snapshot.value as? NSDictionary
                            if data?.count == 0 || data == nil
                            {
                                let access : NSArray = [uid]
                                self.myRootRef.child("Note").child(self.keyid).child("accessuid").setValue([
                                    "userid": access])
                            }
                            else
                            {
                                let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                                var access : NSMutableArray = data["userid"]as! NSMutableArray
                                access.add(uid)
                                self.myRootRef.child("Note").child(self.keyid).child("accessuid").setValue([
                                    "userid": access])
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                                self.navigationController?.pushViewController(controller, animated: true)
                                
                                self.view.removeFromSuperview()
                                self.removeFromParentViewController()
                            }
                    })
                   
                }
        })
    }
}
