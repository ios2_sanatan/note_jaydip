//
//  Collectimage.swift
//  Notes
//
//  Created by Maulesh Kanani on 19/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit

class Collectimage: UICollectionViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var imgbg: UIImageView!
    @IBOutlet weak var imgnote: UIImageView!
    @IBOutlet weak var constantheight: NSLayoutConstraint!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var lbldicription: UILabel!
    @IBOutlet weak var viewcheck: UIView!
}
