//
//  LoginView.swift
//  CreateNote
//
//  Created by Maulesh Kanani on 19/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class LoginView: UIViewController,GIDSignInUIDelegate{
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var strTemp = NSString()
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        spinner.startAnimating()
        self.view.isUserInteractionEnabled = false
        let defaults = UserDefaults.standard
        if defaults.value(forKey: "uid")as? String == nil
        {
          /*  let driveScope = "https://www.googleapis.com/auth/drive"
            let currentScopes : NSArray = GIDSignIn .sharedInstance().scopes as NSArray
            currentScopes.adding("https://www.googleapis.com/auth/drive.file")
            GIDSignIn .sharedInstance().scopes = currentScopes .adding(driveScope)*/
            
            GIDSignIn.sharedInstance().uiDelegate = self
            GIDSignIn.sharedInstance().signIn()

        }
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        let defaults = UserDefaults.standard
        if defaults.value(forKey: "uid")as? String != nil
        {
           let storyboard = UIStoryboard(name: "Main", bundle: nil)
           let controller = storyboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
           self.navigationController?.pushViewController(controller, animated: false)

       }
    }
}


