//
//  Date_timeVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 16/02/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit
@objc protocol selectdate
{
    @objc optional func getdate(arr : String)
}

class Date_timeVC: UIViewController ,FSCalendarDataSource, FSCalendarDelegate, UIGestureRecognizerDelegate
{

    @IBOutlet weak var btndate: UIButton!
    @IBOutlet weak var btntime: UIButton!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var viewcanlander: UIView!
    var date_time = String()
    var dictdata = NSDictionary()
    var delegate : selectdate?
    @IBOutlet var calander: FSCalendar!
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        return formatter
    }()
    override func viewDidLoad() {
        super.viewDidLoad()

         self.calander.select(Date())
         self.calander.scope = .month
        self.calander.accessibilityIdentifier = "calendar"
        viewcanlander.layer.cornerRadius = 10
        viewcanlander.clipsToBounds = true
        
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let datestring = formatter.string(from: date as Date)
        btndate.setTitle(datestring, for: UIControlState.normal)
        lbltime.text = datestring

        let calendar = NSCalendar.current
        let hour = calendar.component(.hour, from: date as Date)
        let minutes = calendar.component(.minute, from: date as Date)
        let time = hour.description + ":" + minutes.description + ":" + "00"
        btntime.setTitle(time, for: UIControlState.normal)
    }
    deinit {
        print("\(#function)")
    }
    
    //MARK:- UIGestureRecognizerDelegate ---------------------------
    
    @IBAction func btnokprassed(_ sender: Any)
    {
        btndate.setTitle(lbltime.text!, for: .normal)
        self.viewcanlander.isHidden = true

    }
    
    @IBAction func btncancelprassed(_ sender: Any)
    {
        self.viewcanlander.isHidden = true
    }
    
    @IBAction func btnselectdate(_ sender: Any) {
        self.viewcanlander.isHidden = false
    }
    @IBAction func btntimeprassed(_ sender: Any)
    {
        self.viewcanlander.isHidden = true
        let datepicker = UIDatePicker()
        datepicker.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width-20, height: 200)
        datepicker.datePickerMode = UIDatePickerMode.time
        let actionsheet = UIAlertController(title: "", message: "\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let OK = UIAlertAction(title: "Done", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let formatter = DateFormatter()
            formatter.dateFormat = "HH:mm"
            let datestring = formatter.string(from: datepicker.date)
            
            self.btntime.setTitle(datestring.appending(":00"), for: UIControlState.normal)
            actionsheet.dismiss(animated: true, completion:nil)
        })
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.default, handler: {
            (alert: UIAlertAction!) -> Void in
            actionsheet.dismiss(animated: true, completion:nil)
        })
        
        actionsheet.addAction(OK)
        actionsheet.addAction(cancel)
        actionsheet.view .addSubview(datepicker)
        self.present(actionsheet, animated: true, completion: nil)

    }
    
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        self.view.layoutIfNeeded()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        print("did select date \(self.dateFormatter.string(from: date))")
        let selectedDates = calendar.selectedDates.map({self.dateFormatter.string(from: $0)})
        print("selected dates is \(selectedDates)")
        lbltime.text = self.dateFormatter.string(from: date)
        if monthPosition == .next || monthPosition == .previous {
            calendar.setCurrentPage(date, animated: true)
        }
    }
    
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        print("\(self.dateFormatter.string(from: calendar.currentPage))")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func btncrossprassed(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func btntickprassed(_ sender: Any) {
        
        let date1 : String = (btndate.titleLabel?.text)!.appending(" ").appending((btntime.titleLabel?.text)!)
        self.delegate?.getdate!(arr: date1)
        let dateString  = NSUtil.convertDateformate(date1)
        let formatter = DateFormatter()
         formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let data = formatter.date(from: dateString!)!
        print(data)
        let delegate = UIApplication.shared.delegate as? AppDelegate
        delegate?.scheduleNotification(at:data)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
}
