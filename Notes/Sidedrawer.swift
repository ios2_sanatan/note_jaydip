//
//  Sidedrawer.swift
//  CreateNote
//
//  Created by Maulesh Kanani on 19/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase

class Sidedrawer: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet var height: NSLayoutConstraint!
    var myarray = NSMutableArray()
    @IBOutlet var tbllabel: UITableView!
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblemail: UILabel!
    @IBOutlet weak var imgprofile: UIImageView!
    var myRootRef: FIRDatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        }
    
    override func viewWillAppear(_ animated: Bool) {
     
        imgprofile.layer.cornerRadius = imgprofile.frame.size.width / 2
        imgprofile.clipsToBounds = true
        let defaults = UserDefaults.standard
        lblname.text = defaults.value(forKey:"name") as? String
        lblemail.text = defaults.value(forKey:"email") as? String
        if defaults.value(forKey:"imgurl") != nil {
            NSUtil.getImageFor(imgprofile, fromURL: defaults.value(forKey:"imgurl") as! String, alterText: nil)
        }
        let uid = defaults.value(forKey:"uid")as! String
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child(uid).child("Labels").observeSingleEvent(of: .value, with: { (snapshot) in
            let data   = snapshot.value as? NSDictionary
            if data?.count == 0 || data == nil
            {
                
            }
            else
            {
                let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                let arr = data.allKeys
                for i in 0 ..< arr.count
                {
                    let key = arr[i] as! String
                    let arrtemp : NSMutableDictionary = data[key] as! NSMutableDictionary
                    self.myarray.add(arrtemp)
                    //DBOperation.executeSQL(String(format: "Insert into create_label('labelname','selectstatus','label_uid','key') Values('%@','%@','%@','%@')",arrtemp["Labelname"]as! String,"false",arrtemp["label_uid"]as! String,key))
                }
                self.tbllabel.reloadData()
            }
        }) { (error) in
            print(error.localizedDescription)
        }
       
    }
    
    @IBAction func btncreatelabelprassed(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  LabelVC = mainStoryboard.instantiateViewController(withIdentifier: "LabelVC") as! LabelVC
        LabelVC.Tag = 1
        let nav = UINavigationController(rootViewController: LabelVC)
        self.mm_drawerController .setCenterView(nav, withCloseAnimation: true, completion: nil)

    }
    @IBAction func btnlogoutprassed(_ sender: Any) {
        
        DBOperation .executeSQL(String(format: "delete from create_label"))
        DBOperation .executeSQL(String(format: "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name='create_label'"))
        
        DBOperation .executeSQL(String(format: "delete from Note_image"))
        DBOperation .executeSQL(String(format: "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name='Note_image'"))
        DBOperation .executeSQL(String(format: "delete from Notes"))
        DBOperation .executeSQL(String(format: "UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name='Notes'"))
        
        let firebaseAuth = FIRAuth.auth()
        do {
            try firebaseAuth?.signOut()
            if let bundle = Bundle.main.bundleIdentifier
            {
                UserDefaults.standard.removePersistentDomain(forName: bundle)
            }
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let  LabelVC = mainStoryboard.instantiateViewController(withIdentifier: "LoginView") as! LoginView
            let nav = UINavigationController(rootViewController: LabelVC)
            self.mm_drawerController .setCenterView(nav, withCloseAnimation: true, completion: nil)

            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    @IBAction func btnnoteprassed(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  userprofileViewController = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        let nav = UINavigationController(rootViewController: userprofileViewController)
        self.mm_drawerController .setCenterView(nav, withCloseAnimation: true, completion: nil)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let numberofcell = myarray.count
        let height1 : CGFloat =  40 * CGFloat(numberofcell)
        height.constant = height1
        return myarray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell: labelTC = tbllabel.dequeueReusableCell(withIdentifier: "labelTC") as! labelTC
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        let dicttemp = myarray[indexPath.row] as! NSDictionary
        cell.LBLNAME.text = dicttemp["Labelname"]as? String
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  NotesVC = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        NotesVC.arrnote = myarray[indexPath.row]as! NSDictionary
        let nav = UINavigationController(rootViewController: NotesVC)
        self.mm_drawerController .setCenterView(nav, withCloseAnimation: true, completion: nil)

    }
}
