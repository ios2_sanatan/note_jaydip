//
//  LabelVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 28/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
@objc protocol selectlabelnamedelegate
{
    @objc optional func labelname(arr : NSMutableArray)
}
class LabelVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tbllabel: UITableView!
    @IBOutlet weak var txtlabelname: UITextField!
    var arraydata = NSMutableArray()
    var arrayItem = NSMutableArray()
    var note_uid = String()
    var Tag = Int()
    var delegate : selectlabelnamedelegate?
    var myRootRef: FIRDatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        arraydata = DBOperation.selectData(String(format: "select * from create_label"))
        self.tbllabel.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
       
    }
    //MARK:- button touch up methods-----------------------------------------
    
    @IBAction func btnleftprassed(_ sender: Any) {
        if Tag == 1
        {
             self.mm_drawerController .toggle(MMDrawerSide.left, animated: true, completion: nil)
        }
        else
        {
            self.delegate?.labelname!(arr: arrayItem)
            _ = navigationController?.popViewController(animated: true)
        }
    }
    @IBAction func btntickprassed(_ sender: Any) {
        
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        
        let autono : NSArray = DBOperation.selectData(String(format:"select * from create_label"))
        let Randomnumber : Int = Int(arc4random_uniform(UInt32(10000)))
        let number :Int = Int (arc4random() % 16)
        let abc : Int = autono.count + 1;
        let final : String = "lbl" + abc.description + Randomnumber.description + number.description
        if txtlabelname.text != ""
        {
            myRootRef = FIRDatabase.database().reference()
            self.myRootRef.child(uid!).child("Labels").childByAutoId().setValue([
                "Labelname": txtlabelname.text!,
                "label_uid": final,
                "selectstatus":false])
            DBOperation.executeSQL(String(format: "Insert into create_label('labelname','selectstatus','label_uid') Values('%@','%@','%@')",txtlabelname.text!,"false",final))
        }
        txtlabelname.text = ""
        arraydata = DBOperation.selectData(String(format: "select * from create_label"))
        self.tbllabel.reloadData()
        self.view.endEditing(true)
    
    }
    
    func getdatafirebase() -> Void
    {
        arraydata.removeAllObjects()
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child(uid!).child("Labels").observeSingleEvent(of: .value, with: { (snapshot) in
            
            let data   = snapshot.value as? NSDictionary
            if data?.count == 0 || data == nil
            {
                
            }
            else
            {
                let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                let arr = data.allKeys
                for i in 0 ..< arr.count
                {
                    let key = arr[i] as! String
                    let arrtemp : NSMutableDictionary = data[key] as! NSMutableDictionary
                    arrtemp.setValue(key, forKey: "key")
                    self.arraydata.add(arrtemp)
                }
                self.tbllabel.reloadData()
            }
        }) { (error) in
            print(error.localizedDescription)
        }

    }
    //MARK:- tableview delegate methods---------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arraydata.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:Labelcell = self.tbllabel.dequeueReusableCell(withIdentifier: "Labelcell") as! Labelcell
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        let dicttemp = arraydata[indexPath.row]as! NSDictionary
        cell.btncheck.tag = indexPath.row
        cell.lblname.text = dicttemp["labelname"] as? String
        cell.btncheck.addTarget(self, action: #selector(LabelVC.btnckeckPrassed), for: UIControlEvents.touchUpInside)
        for i in 0..<arrayItem.count
        {
            if arrayItem[i]as? String == dicttemp["labelname"]as? String
            {
                cell.btncheck .isSelected = true

            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell: Labelcell = self.tbllabel.cellForRow(at: indexPath) as! Labelcell
        let dicttemp = arraydata[indexPath.row]as! NSDictionary
        if cell.btncheck .isSelected == true
        {
            cell.btncheck .isSelected = false
            arrayItem.remove(dicttemp["labelname"]as! String)
            
        }else{
            cell.btncheck .isSelected = true
            arrayItem.add(dicttemp["labelname"]as! String)
        }
    }
    func btnckeckPrassed(sender: UIButton)
    {
        let indexPath : IndexPath = IndexPath(item: sender.tag, section: 0)
        let cell: Labelcell = self.tbllabel.cellForRow(at: indexPath) as! Labelcell
        let dicttemp = arraydata[indexPath.row]as! NSDictionary
        
            if cell.btncheck .isSelected == true
            {
                cell.btncheck .isSelected = false
                arrayItem.remove(dicttemp["labelname"]as! String)
                
            }else{
                cell.btncheck .isSelected = true
                arrayItem.add(dicttemp["labelname"]as! String)
           }
    }
}
