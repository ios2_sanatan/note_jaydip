//
//  Labelcell.swift
//  Notes
//
//  Created by Maulesh Kanani on 28/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit

class Labelcell: UITableViewCell {
    
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var btncheck: UIButton!

    @IBOutlet weak var btnyes: UIButton!
    @IBOutlet weak var btnno: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
