//
//  AppDelegate.swift
//  Notes
//
//  Created by Maulesh Kanani on 26/12/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,GIDSignInDelegate, GIDSignInUIDelegate  {
    
    var window: UIWindow?
    var myAuth: GTMFetcherAuthorizationProtocol?
    var myRootRef: FIRDatabaseReference!
    var uid : String?
    var mmdrawercontroller : MMDrawerController! = nil
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        mmdrawercontroller = self.window?.rootViewController as! MMDrawerController
        mmdrawercontroller .setMaximumLeftDrawerWidth(260.0, animated: true, completion: nil)
        mmdrawercontroller .closeDrawerGestureModeMask = MMCloseDrawerGestureMode.tapNavigationBar
        mmdrawercontroller .closeDrawerGestureModeMask = MMCloseDrawerGestureMode.all
        mmdrawercontroller .setDrawerVisualStateBlock { (drawerController : MMDrawerController?, drawerSide : MMDrawerSide, percentVisible : CGFloat) in
            var block :  MMDrawerControllerDrawerVisualStateBlock?
            block = ((MMExampleDrawerVisualStateManager .shared()).drawerVisualStateBlock(for: drawerSide))
            if (block != nil)
            {
                block!(drawerController,drawerSide,percentVisible)
            }
        }
        
        DBOperation .checkCreateDB()
        FIRApp .configure()
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
       
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound]) {(accepted, error) in
            if !accepted {
                print("Notification access denied.")
            }
        }

        return true
    }
    
    // Notification
    
    func scheduleNotification(at date: Date) {
        
        let calendar = Calendar(identifier: .gregorian)
        let components = calendar.dateComponents(in: .current, from: date)
        let newComponents = DateComponents(calendar: calendar, timeZone: .current, month: components.month, day: components.day, hour: components.hour, minute: components.minute)
        
        let trigger = UNCalendarNotificationTrigger(dateMatching: newComponents, repeats: false)
    
        let content = UNMutableNotificationContent()
        content.title = "Notes..."
        content.body = "Just a reminder to read your tutorial over at appcoda.com!"
        content.sound = UNNotificationSound.default()
       
        let request = UNNotificationRequest(identifier: "textNotification", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request) {(error) in
            if let error = error {
                print("Uh oh! We had an error: \(error)")
            }
        }
    }
    
    //MARK:- google authentication methods--------------------------
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            return GIDSignIn.sharedInstance().handle(url,
                                                        sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                        annotation: [:])
    }

    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                    sourceApplication: sourceApplication,
                                                    annotation: annotation)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
      
        let myDelegate = UIApplication.shared.delegate as? LoginView
        myDelegate?.spinner.startAnimating()
        let defaults = UserDefaults.standard
        if (error == nil) {
            
            defaults.set(user.profile.name, forKey: "name")
            defaults.set(user.profile.email, forKey: "email")
            let dimension = round(100 * UIScreen.main.scale)
            let pic = user.profile.imageURL(withDimension: UInt(dimension))
            defaults.set(pic?.absoluteString, forKey: "imgurl")
            defaults.synchronize()
            myAuth = user.authentication.fetcherAuthorizer()
            
           guard let authentication = user.authentication else { return }
          let credential = FIRGoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                          accessToken: authentication.accessToken)
            
           FIRAuth.auth()?.signIn(with: credential) { (user1, error) in
           if let error = error {
              print(error.localizedDescription)
              return
           } else {
            
             self.myRootRef = FIRDatabase.database().reference()
             self.myRootRef.child("USERS").child((user1?.uid)!).setValue(["User": user.profile.email, "userid":user1?.uid])
             self.uid = user1?.uid
            self.notelabel(uid: self.uid!)
            self.getnote(uid : self.uid!)
            self.notification(uid: self.uid!)
            defaults.set(self.uid, forKey: "uid")
            defaults.synchronize()
            
            }
        }
    }
        
}
    
    func notification(uid : String) -> Void
    {
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child(uid).child("Notification").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    let defaults = UserDefaults.standard
                    defaults.set(0, forKey: "count")
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let  LabelVC = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                    let nav = UINavigationController(rootViewController: LabelVC)
                    self.mmdrawercontroller .setCenterView(nav, withCloseAnimation: true, completion: nil)

                }
                else
                {
                    let defaults = UserDefaults.standard
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                    let arr : NSArray = data.allKeys as NSArray
                    defaults.set(arr.count, forKey: "count")
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let  LabelVC = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                    let nav = UINavigationController(rootViewController: LabelVC)
                    self.mmdrawercontroller .setCenterView(nav, withCloseAnimation: true, completion: nil)
                }
        })
        let myDelegate = UIApplication.shared.delegate as? LoginView
        myDelegate?.spinner.stopAnimating()
    }
    
    func notelabel(uid : String) -> Void
    {
        DBOperation .executeSQL(String(format: "delete from create_label"))
        
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child(uid).child("Labels").observeSingleEvent(of: .value, with: { (snapshot) in
            let data   = snapshot.value as? NSDictionary
            if data?.count == 0 || data == nil
            {
                
            }
            else
            {
                let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                let arr = data.allKeys
                for i in 0 ..< arr.count
                {
                    let key = arr[i] as! String
                    let arrtemp : NSMutableDictionary = data[key] as! NSMutableDictionary
                    
                      DBOperation.executeSQL(String(format: "Insert into create_label('labelname','selectstatus','label_uid','key') Values('%@','%@','%@','%@')",arrtemp["Labelname"]as! String,"false",arrtemp["label_uid"]as! String,key))
                }
            }
        }) { (error) in
            print(error.localizedDescription)
        }

    }
    func getnote(uid : String) -> Void
    {
        DBOperation .executeSQL(String(format: "delete from Note_image"))
        DBOperation .executeSQL(String(format: "delete from checklist"))
        DBOperation .executeSQL(String(format: "delete from Notes"))
        DBOperation .executeSQL(String(format: "delete from Note_audio"))
     

        myRootRef = FIRDatabase.database().reference()
        myRootRef.child("Note").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let  LabelVC = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                    let nav = UINavigationController(rootViewController: LabelVC)
                    self.mmdrawercontroller .setCenterView(nav, withCloseAnimation: true, completion: nil)
                    
                }
                else
                {
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                    let arr = data.allKeys
                    for i in 0 ..< arr.count
                    {
                       
                        let key = arr[i] as! String
                        let arrtemp : NSMutableDictionary = data[key] as! NSMutableDictionary
                        let accessuser : NSDictionary = arrtemp["accessuid"]as! NSDictionary
                        let notedata : NSDictionary = arrtemp["data"]as! NSDictionary
                        let arruser : NSArray = accessuser["userid"]as! NSArray
                        for j in 0..<arruser.count
                        {
                            if arruser[j]as! String == uid
                            {
                                let arrlabel : NSArray = notedata["Labels"]as! NSArray
                                let arrimages : NSArray = notedata["Images"]as! NSArray
                                let arrcheck : NSArray = notedata["checklist"]as! NSArray
                                
                                self.addimages(arr: arrimages, noteuid: notedata["note_uid"]as! String, key: key)
                                self.checklist(arr: arrcheck, noteuid: notedata["note_uid"]as! String, key: key)
                                let audio : String = notedata["audio"]as! String
                                if audio != " "
                                {
                                    let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                                    let storageRef = FIRStorage.storage().reference()
                                    let starsRef = storageRef.child("audio/" + audio)
                                    
                                    let fileURL = documentsDirectoryURL.appendingPathComponent(audio)
                                    let downloadTask = starsRef.write(toFile: fileURL) { url, error in
                                        if let error = error {
                                            print(error.localizedDescription)
                                            
                                        } else
                                        {
                                            DBOperation.executeSQL(String(format: "Insert into Note_audio('noite_uid','audio_name','key') Values('%@','%@','%@')",(notedata["note_uid"]as? String)!,(url?.absoluteString)!,key))
                                        }
                                    }
                                }
                                let display : String = notedata["display"]as! String
                                let Height : Float = Float(notedata["cellheight"]as! String)!as Float
                                DBOperation.executeSQL(String(format: "Insert into Notes('Title','SubTitle','note_uid','label','key','hexa','cellheight','display') Values('%@','%@','%@','%@','%@','%@','%f','%@')",notedata["Title"]as! String,notedata["description"]as! String,notedata["note_uid"]as! String,arrlabel.componentsJoined(by: ","),key,notedata["color"]as! String,Height,display))
                            }
                        }
                    }
                }
            })
        { (error) in
            print(error.localizedDescription)
        }
    }

    func checklist(arr : NSArray,noteuid:String , key: String)
    {
        for j in 0 ..< arr.count
        {
            let dict = arr[j]as! NSDictionary
            if dict["checklist"]as! String != ""
            {
                DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",noteuid,dict["checklist"]as! String,dict["flag"]as! String,key))

            }
        }
    }
    
    func addimages(arr : NSArray,noteuid:String , key: String)
    {
        var urlString = String()
        for j in 0 ..< arr.count
        {
            if  arr[j]as! String != ""
            {
                let name = arr[j]as! String
                let storageRef = FIRStorage.storage().reference()
                let starsRef = storageRef.child("imags/" + name)
                
                starsRef.data(withMaxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        
                    }
                    else
                    {
                        let image = UIImage(data: data!)
                        
                        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                        let fileURL = documentsDirectoryURL.appendingPathComponent(arr[j]as! String)
                        urlString = fileURL.path
                        
                        if !FileManager.default.fileExists(atPath: fileURL.path)
                        {
                            if (try? UIImageJPEGRepresentation(image!, 0.1)!.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])) != nil
                            {
                                print("file saved")
                            }
                            else
                            {
                                print("error saving file")
                            }
                        }
                        DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name','key') Values('%@','%@','%@')",noteuid,urlString,key))
                    }
                }
            }
        }
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if application.applicationState == .inactive || application.applicationState == .background {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navigationController = self.window?.rootViewController as? UINavigationController
            let destinationController = storyboard.instantiateViewController(withIdentifier: "NotesVC") as? NotesVC
            navigationController?.pushViewController(destinationController!, animated: false)
        }
    }
}

