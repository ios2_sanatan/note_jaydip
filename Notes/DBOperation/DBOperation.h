//
//  DBOperation.h
//  Sales Tracking
//
//  Created by Maulesh Kanani on 31/05/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>
@interface DBOperation : NSObject
{
    
}

+(void)OpenDatabase:(NSString*)path;
+(void)checkCreateDB;
+(BOOL) executeSQL:(NSString *)sqlTmp;
+(NSMutableArray*) selectData:(NSString *)sql;
@end
