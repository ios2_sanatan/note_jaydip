//
//  NSUtil.m
//  StoreGuide
//
//  Created by Maulesh Kanani on 11/02/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//


#import "NSUtil.h"
#import "WebImageOperations.h"
#import "PreventBackup.h"
#import <sys/utsname.h>
#import <sys/sysctl.h>


@implementation NSUtil

+(NSString*)getJsonStringFromDictionary:(NSDictionary*)dictionary{
    NSString *jsonString=@"";
    @try {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (!jsonData) {
            NSLog(@"JsonError: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }

    }
    @catch (NSException *exception) {
        NSLog(@"exception: %@", exception);
    }
    @finally {
        
    }
    return jsonString;
}
+(NSArray*)shortDirectoryArray:(NSArray*)arrayToShort ByKey:(NSString*)key{
    @try {
        return [[arrayToShort sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = [((NSDictionary*)a) valueForKey:key];
            NSString *second = [((NSDictionary*)b) valueForKey:key];
            return [first compare:second];
        }] mutableCopy];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

+(void)getImageForView:(UIImageView *)imageView fromURL:(NSString *)imgUrl alterText:(NSString*)alterText
{
    @try
    {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

        if(!imgUrl ||imgUrl.length==0)
        {
            return;
        }
        
        imgUrl = [imgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arratemp = [imgUrl componentsSeparatedByString:@"/"];
        
        NSString *imgPath = [NSString stringWithFormat:@"%@/%@",docDir,[arratemp lastObject]];
        
        NSURL *pathURL= [NSURL fileURLWithPath:imgPath];
        
        [PreventBackup addSkipBackupAttributeToItemAtURL:pathURL];
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        
        BOOL isFileExist = [filemgr fileExistsAtPath:imgPath];
        
        if(!isFileExist)
        {
            __block UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [indicatorView setColor:[UIColor blackColor]];
            indicatorView.frame=CGRectMake((imageView.frame.size.width-37)/2, (imageView.frame.size.height-37)/2, 37, 37);
            [indicatorView startAnimating];
            [imageView addSubview:indicatorView];
            
            [WebImageOperations processImageDataWithURLString:imgUrl andBlock:^(NSData *imageData)
            {
                [imageData writeToFile:imgPath atomically:YES];
                [indicatorView removeFromSuperview];
                
                if([imageView isKindOfClass:[UIImageView class]])
                {
                    UIImage *img = [UIImage imageWithData:imageData];
                    [self cropImage:img forView:imageView];
                }
                else if([imageView isKindOfClass:[UIButton class]])
                {
                    [((UIButton*)imageView) setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
                }
            }];
        }
        else
        {
            if([imageView isKindOfClass:[UIImageView class]])
            {
                UIImage *img = [UIImage imageWithContentsOfFile:imgPath];
                [self cropImage:img forView:imageView];
            }
            else if([imageView isKindOfClass:[UIButton class]])
            {
                [((UIButton*)imageView) setImage:[UIImage imageWithContentsOfFile:imgPath] forState:UIControlStateNormal];
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception :%@",exception);
    }
    @finally {
        
    }
}

+(void)getImageForView1:(UIView*)imageView fromURL:(NSString *)imgUrl alterText:(NSString*)alterText{
    @try {
        
        // Get an image from the URL below
        // Let's save the file into Document folder.
        // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
        // NSString *deskTopDir = @"/Users/kiichi/Desktop";
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        // If you go to the folder below, you will find those pictures
        //        NSLog(@"CHECK URL%@",imgUrl);
        
        
        //        NSLog(@"saving jpeg");
        if(!imgUrl ||imgUrl.length==0)
        {
            
            return;
        }
        
        // ImgUrl = [ImgUrl stringByReplacingOccurrencesOfString:@" " withString:@""];
        imgUrl = [imgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSArray *arratemp = [imgUrl componentsSeparatedByString:@"/"];
        NSString *imgPath = [NSString stringWithFormat:@"%@/%@",docDir,[arratemp lastObject]];
        NSURL *pathURL= [NSURL fileURLWithPath:imgPath];
        [PreventBackup addSkipBackupAttributeToItemAtURL:pathURL];
        NSFileManager *filemgr = [NSFileManager defaultManager];
        BOOL isFileExist = [filemgr fileExistsAtPath:imgPath];
        
        if(!isFileExist)
        {
            //    UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ImgUrl]]];
            __block UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            indicatorView.frame=CGRectMake((imageView.frame.size.width-37)/2, (imageView.frame.size.height-37)/2, 37, 37);
            [indicatorView setColor:[UIColor blackColor]];
            [indicatorView startAnimating];
            [imageView addSubview:indicatorView];
            [WebImageOperations processImageDataWithURLString:imgUrl andBlock:^(NSData *imageData) {
                //                NSLog(@"webimage1");
                [imageData writeToFile:imgPath atomically:YES];
                [indicatorView removeFromSuperview];
                
                if([imageView isKindOfClass:[UIImageView class]])
                {
                    UIImage *image = [UIImage imageWithData:imageData];
                    //                    ((UIImageView*)imageView).image = image;
                    //                    [((UIImageView*)imageView) setImage:image];
                    [NSUtil cropImage:image forView:((UIImageView*)imageView)];
                }
                //                else if([imageView isKindOfClass:[UIButton class]]){
                //                    [((UIButton*)imageView) setBackgroundImage:[UIImage imageWithData:imageData] forState:UIControlStateNormal];
                //                }
                //                NSLog(@"webimage2");
            }];
            
        }
        else
        {
            //            NSLog(@"webimage3");
            if([imageView isKindOfClass:[UIImageView class]])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIImage *image =[UIImage imageWithContentsOfFile:imgPath];
                    //                ((UIImageView*)imageView).image = image;
                    //                [((UIImageView*)imageView) setImage:image];
                    [NSUtil cropImage:image forView:((UIImageView*)imageView)];
                });
                
            }
            //            else if([imageView isKindOfClass:[UIButton class]]){
            //                [((UIButton*)imageView) setImage:[UIImage imageWithContentsOfFile:imgPath] forState:UIControlStateNormal];
            //
            //            }
            //            NSLog(@"webimage5");
            
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception :%@",exception);
    }
    @finally {
        
    }
}


+(void)cropImage:(UIImage *)originalImage forView:(UIImageView *)imageView
{
    @try {
        if (originalImage.size.width==originalImage.size.height ||(originalImage.size.width==imageView.frame.size.width&&originalImage.size.height==imageView.frame.size.height)) {
            imageView.image=originalImage;
            return;
        }
        
        if(imageView.frame.size.width/imageView.frame.size.height == originalImage.size.width/originalImage.size.height){
            imageView.image=originalImage;
            return;
        }
        
        originalImage = [self fixrotation:originalImage];
        float y = 0;
        float x=(originalImage.size.width -(imageView.frame.size.width*2))/2;
        CGSize size = originalImage.size;
        
        if (imageView.frame.size.width<originalImage.size.width && imageView.frame.size.height<originalImage.size.height) {
            
            if (originalImage.size.width<originalImage.size.height) {
                size = CGSizeMake(originalImage.size.width, imageView.frame.size.height*(originalImage.size.width/imageView.frame.size.width));
                if (size.height>originalImage.size.height) {
                    size = CGSizeMake(imageView.frame.size.width*(originalImage.size.height/imageView.frame.size.height), originalImage.size.height);
                }
                x=0;
                y=(originalImage.size.height -size.height)/2;
                
            }else{
                size = CGSizeMake(imageView.frame.size.width*(originalImage.size.height/imageView.frame.size.height), originalImage.size.height);
                
                if (size.width>originalImage.size.width) {
                    size = CGSizeMake(originalImage.size.width, imageView.frame.size.height*(originalImage.size.width/imageView.frame.size.width));
                }
                x=(originalImage.size.width -size.width)/2;
                y=0;
                
            }
        }else{
            imageView.image=originalImage;
            return;
        }
        
        CGRect clippedRect  = CGRectMake(x, y, size.width, size.height);
        
        CGImageRef imageRef = CGImageCreateWithImageInRect([originalImage CGImage], clippedRect);
        UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
        // scale:1.0 orientation:originalImage.imageOrientation
        CGImageRelease(imageRef);
        
        //        newImage = [[UIImage alloc] initWithCGImage:newImage.CGImage scale:0.0f orientation:UIImageOrientationLeftMirrored];
        imageView.image = newImage;
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

+(UIImage*)cropImageFromMiddle:(UIImage *)originalImage
{
    @try {
        if (originalImage.size.width==originalImage.size.height) {
            return originalImage;
        }
        originalImage = [self fixrotation:originalImage];
        float size=0;
        float y = 0;
        float x=0;
        if (originalImage.size.width<originalImage.size.height) {
            size =originalImage.size.width;
            y=(originalImage.size.height-originalImage.size.width)/2;
        }else{
            size =originalImage.size.height;
            x=(originalImage.size.width-originalImage.size.height)/2;
            
        }
        
        CGRect clippedRect  = CGRectMake(x, y, size, size);
        
        CGImageRef imageRef = CGImageCreateWithImageInRect([originalImage CGImage], clippedRect);
        UIImage *newImage   = [UIImage imageWithCGImage:imageRef];
        // scale:1.0 orientation:originalImage.imageOrientation
        CGImageRelease(imageRef);
        
        //        newImage = [[UIImage alloc] initWithCGImage:newImage.CGImage scale:0.0f orientation:UIImageOrientationLeftMirrored];
        
        return newImage;
    }
    @catch (NSException *exception)
    {
    }
    @finally
    {
    }
    return nil;
}

+(void)storeImageToDocumentDirectry :(NSString *)imgURL
{
    @try
    {
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        if(!imgURL ||imgURL.length==0)
        {
            return;
        }
        
        imgURL = [imgURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSArray *arratemp = [imgURL componentsSeparatedByString:@"/"];
        
        NSString *imgPath = [NSString stringWithFormat:@"%@/%@",docDir,[arratemp lastObject]];
        
        NSURL *pathURL= [NSURL fileURLWithPath:imgPath];
        
        [PreventBackup addSkipBackupAttributeToItemAtURL:pathURL];
        
        NSFileManager *filemgr = [NSFileManager defaultManager];
        
        BOOL isFileExist = [filemgr fileExistsAtPath:imgPath];
        
        if(!isFileExist)
        {
            [WebImageOperations processImageDataWithURLString:imgURL andBlock:^(NSData *imageData){
                 [imageData writeToFile:imgPath atomically:YES];
            }];
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
        {
    }
}

+(UIImage *)generateThumbImage : (NSURL *)filepath
{
    @try
    {
        AVAsset *asset = [AVAsset assetWithURL:filepath];
        AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
        imageGenerator.appliesPreferredTrackTransform = YES;
        CMTime time = [asset duration];
        time.value = 0;
        CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
        UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
        CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
        
        UIImage *rotatedImage1 = [[UIImage alloc] initWithCGImage:thumbnail.CGImage];
        return rotatedImage1;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
}

+(UIImage*)getImageFromURL:(NSString *)imgUrl
{
    @try {
        
        // Get an image from the URL below
        // Let's save the file into Document folder.
        // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
        // NSString *deskTopDir = @"/Users/kiichi/Desktop";
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        // If you go to the folder below, you will find those pictures
        //        NSLog(@"%@",docDir);
        
        
        //        NSLog(@"saving jpeg");
        //
        imgUrl = [imgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSArray *arratemp = [imgUrl componentsSeparatedByString:@"/"];
        NSString *imgPath = [NSString stringWithFormat:@"%@/%@",docDir,[arratemp lastObject]];
        NSURL *pathURL= [NSURL fileURLWithPath:imgPath];
        [PreventBackup addSkipBackupAttributeToItemAtURL:pathURL];
        NSFileManager *filemgr = [NSFileManager defaultManager];
        BOOL isFileExist = [filemgr fileExistsAtPath:imgPath];
        if(!isFileExist)
        {
            
            NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
            [imgData writeToFile:imgPath atomically:YES];
            return [UIImage imageWithData:imgData];
        }
        else
        {
            return [UIImage imageWithContentsOfFile:imgPath];
        }
    }
    @catch (NSException *exception) {
        NSLog(@"Exception :%@",exception);
    }
    @finally {
        
    }
}

+(NSString*)getDataFromURL:(NSString *)imgUrl
{
    @try {
        
        // Get an image from the URL below
        // Let's save the file into Document folder.
        // You can also change this to your desktop for testing. (e.g. /Users/kiichi/Desktop/)
        
        NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        
        // If you go to the folder below, you will find those pictures
        NSLog(@"%@",docDir);
        
        imgUrl = [imgUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSArray *arratemp = [imgUrl componentsSeparatedByString:@"/"];
        NSString *imgPath = [NSString stringWithFormat:@"%@/%@",docDir,[arratemp lastObject]];
        NSURL *pathURL= [NSURL fileURLWithPath:imgPath];
        [PreventBackup addSkipBackupAttributeToItemAtURL:pathURL];
        NSFileManager *filemgr = [NSFileManager defaultManager];
        [filemgr fileExistsAtPath:imgPath];
        
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgUrl]];
        [data writeToFile:imgPath atomically:YES];
        return imgPath;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception :%@",exception);
    }
    @finally {
        
    }
}

+(NSString*)convertDate:(NSString*)dateToConvert
{
    NSString *dateString = @"";
    @try {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateToConvert];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        dateString = [dateFormat stringFromDate:date];
        return dateString;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

+(NSString*)convertDatewise:(NSString*)dateToConvert
{
    NSString *dateString = @"";
    @try {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:dateToConvert];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"dd-MM-yyyy"];
        dateString = [dateFormat stringFromDate:date];
        return dateString;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


+(NSString*)convertDateformate:(NSString*)dateToConvert
{
    NSString *dateString = @"";
    @try {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
        NSDate *date = [dateFormat dateFromString:dateToConvert];
        
        // Convert date object to desired output format
        [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        dateString = [dateFormat stringFromDate:date];
        return dateString;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
    
}


+(NSString*)convertDate:(NSString*)dateToConver dateFormate:(NSString*)dateFormate dateFormateToConvert:(NSString*)dateFormateToConvert{
    @try {
        NSDateFormatter* formatterUtc = [[NSDateFormatter alloc] init];
        [formatterUtc setDateFormat:dateFormate];
        
        NSDate* utcDate = [formatterUtc dateFromString:dateToConver];
        
        NSTimeZone *currentTimeZone = [NSTimeZone localTimeZone];
        NSTimeZone *utcTimeZone = [NSTimeZone timeZoneWithAbbreviation:@"UTC"];
        
        NSInteger currentGMTOffset = [currentTimeZone secondsFromGMTForDate:utcDate];
        NSInteger gmtOffset = [utcTimeZone secondsFromGMTForDate:utcDate];
        NSTimeInterval gmtInterval = currentGMTOffset - gmtOffset;
        
        NSDate *destinationDate = [[NSDate alloc] initWithTimeInterval:gmtInterval sinceDate:utcDate];
        
        NSDateFormatter *dateFormatters = [[NSDateFormatter alloc] init];
        [dateFormatters setDateFormat:dateFormateToConvert];
        [dateFormatters setTimeZone:[NSTimeZone systemTimeZone]];
        NSString *dateStr = [dateFormatters stringFromDate: destinationDate];
//        NSLog(@"Converted Date : %@", dateStr);
        return dateStr;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }    
}

+(NSString*)convertDateTimeStamp:(NSString*)timestamp DateFormat:(NSString*)dateFormat
{
    @try
    {
        double unixTime=[timestamp doubleValue];
        NSTimeInterval _interval=unixTime;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setLocale:[NSLocale currentLocale]];
        [_formatter setDateFormat:dateFormat];
        NSString *_date=[_formatter stringFromDate:date];
        return _date;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


+ (UIImage *) cropImage:(UIImage *)originalImage
{
    CGRect cropRect = CGRectMake(0, 0, originalImage.size.width , originalImage.size.height);
    
    CGAffineTransform rectTransform;
    switch (originalImage.imageOrientation)
    {
        case UIImageOrientationLeft:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(M_PI_2), 0, -originalImage.size.height);
            break;
        case UIImageOrientationRight:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI_2), -originalImage.size.width, 0);
            break;
        case UIImageOrientationDown:
            rectTransform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(-M_PI), -originalImage.size.width, -originalImage.size.height);
            break;
        default:
            rectTransform = CGAffineTransformIdentity;
    };
    rectTransform = CGAffineTransformScale(rectTransform, originalImage.scale, originalImage.scale);
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([originalImage CGImage], CGRectApplyAffineTransform(cropRect, rectTransform));
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:originalImage.scale orientation:originalImage.imageOrientation];
    CGImageRelease(imageRef);
    
    //Now want to scale down cropped image!
    //want to multiply frames by 2 to get retina resolution
    CGRect scaledImgRect =  CGRectMake(0, 0, originalImage.size.width/2 , originalImage.size.height/2);
    
    UIGraphicsBeginImageContextWithOptions(scaledImgRect.size, NO, [UIScreen mainScreen].scale);
    
    [result drawInRect:scaledImgRect];
    
    UIImage *scaledNewImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledNewImage;
    
}

+(UIActivityIndicatorView *)setSpinner :(UIActivityIndicatorView *)spinner
{
    @try
    {
        float width = [UIScreen mainScreen].bounds.size.width;
        float height = [UIScreen mainScreen].bounds.size.height;
        
        spinner.frame = CGRectMake((width-37)/2, (height-37)/2, 37, 37);
        
        return spinner;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
}

+(UITextField *) addPaddingOnTextField :(UITextField *)txtField
{
    @try
    {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, txtField.frame.size.height)];
        txtField.leftView = paddingView;
        txtField.leftViewMode = UITextFieldViewModeAlways;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
    return txtField;
}

+(void)ToastMessage:(NSString *)message
{
    UIAlertView *toast = [[UIAlertView alloc] initWithTitle:nil                                                        message:message delegate:nil cancelButtonTitle:nil                                               otherButtonTitles:nil, nil];
    
    toast.backgroundColor=[UIColor blackColor];
    [toast show];
    int duration = 1; 
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [toast dismissWithClickedButtonIndex:0 animated:YES];
    });
}

+(UITextField *)setPlaceHolderColorOnTextField :(UITextField *)txtField
{
    [txtField setTextColor:[UIColor blackColor]];
    [txtField setValue:[UIColor blackColor] forKeyPath:@"_placeholderLabel.textColor"];
    return txtField;
}

+(UIButton *)setBorderRadius:(id)view
{
    [view layer].borderWidth = 1.0;
    [view layer].borderColor = [[UIColor colorWithRed:143.0/255.0 green:143.0/255.0 blue:143.0/255.0 alpha:1.0] CGColor];
    [view setClipsToBounds:true];
    
    return view;
}

+(CGSize)dynamicSizeFromText :(NSString *)strText withFont:(UIFont *)font withMaxSize:(CGSize)maxSize
{
    NSDictionary *attributes = @{NSFontAttributeName:font};
    
    CGRect rect = [strText boundingRectWithSize:maxSize
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:attributes
                                        context:nil];
    return rect.size;
}

+(NSMutableArray *) sortingArray:(NSMutableArray *)arrSorted withKey:(NSString *)strKey
{
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:strKey ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *arrSortDescriptor = [NSArray arrayWithObject:sortDescriptor];
    
    [arrSorted sortUsingDescriptors:arrSortDescriptor];
    
    return arrSorted;
}

+(UIImage *)fixrotation:(UIImage *)image
{
    if (image.imageOrientation == UIImageOrientationUp) return image;
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (image.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (image.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (image.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

+(NSString *)getRoundedMinute:(NSString *)strTime
{
    @try
    {
        NSString *strHour = [[strTime componentsSeparatedByString:@":"] objectAtIndex:0];
        NSString *strMinute = [[strTime componentsSeparatedByString:@":"] objectAtIndex:1];
        
        if ([strMinute intValue] == 0 || [strMinute intValue] == 15 || [strMinute intValue] == 30 || [strMinute intValue] == 45)
        {
            return strTime;
        }
        else if ([strMinute intValue] > 0 && [strMinute intValue] < 15)
        {
            strMinute = @"15";
        }
        else if ([strMinute intValue] > 15 && [strMinute intValue] < 30)
        {
            strMinute = @"30";
        }
        else if ([strMinute intValue] > 30 && [strMinute intValue] < 45)
        {
            strMinute = @"45";
        }
        else if ([strMinute intValue] > 45)
        {
            strMinute = @"00";
            strHour = [NSString stringWithFormat:@"%d",[strHour intValue]+1];
        }
        
        strTime = [NSString stringWithFormat:@"%@:%@",strHour,strMinute];
        
        return strTime;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
}

+(NSString*)convertDateToDeviceTime:(NSString*)msgDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate *date = [dateFormatter dateFromString:msgDate];
    
    NSDateFormatter *dateFormatterDest = [[NSDateFormatter alloc] init];
    [dateFormatterDest setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSDate *dateNew = [NSDate dateWithTimeIntervalSinceNow:date.timeIntervalSinceNow];
    NSString *dateString = [dateFormatterDest stringFromDate:dateNew];
    
    return dateString;
}

- (UIColor *)colorWithHexString:(NSString *)hexString
{
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(NSString *) getNumberFormate :(int)intValue
{
    NSNumberFormatter *num = [[NSNumberFormatter alloc] init];
    [num setNumberStyle: NSNumberFormatterCurrencyStyle];
    [num setCurrencyCode:@""];
    [num setInternationalCurrencySymbol:@""];
    [num setMaximumFractionDigits:0];
    num.positiveFormat = [num.positiveFormat
                          stringByReplacingOccurrencesOfString:@"¤" withString:@""];
    num.negativeFormat = [num.negativeFormat
                          stringByReplacingOccurrencesOfString:@"¤" withString:@""];
    
    NSString *numberAsString = [num stringFromNumber:[NSNumber numberWithInt:intValue]];
    
    return numberAsString;
}

@end

@implementation NSDictionary (JSONKitSerializing)

- (NSString *)JSONString
{
    NSString *jsonString=@"";
    
    @try
    {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (!jsonData)
        {
            NSLog(@"JsonError: %@", error);
        }
        else
        {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception: %@", exception);
    }
    @finally
    {
    }
    return jsonString;
}

@end
@implementation NSArray(JSONKitSerializing)

- (NSString *)JSONString{
    
    NSString *jsonString=@"";
    @try {
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                           options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                             error:&error];
        
        if (!jsonData) {
            NSLog(@"JsonError: %@", error);
        } else {
            jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
    }
    @catch (NSException *exception)
    {
        NSLog(@"exception: %@", exception);
    }
    @finally
    {
        
    }
    return jsonString;
}

@end
