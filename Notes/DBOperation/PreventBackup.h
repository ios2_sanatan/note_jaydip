//
//  PreventBackup.h
//  StoreGuide
//
//  Created by Maulesh Kanani on 11/02/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PreventBackup : NSObject
+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
@end
