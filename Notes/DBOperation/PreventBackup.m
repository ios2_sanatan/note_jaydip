//
//  PreventBackup.m
//  StoreGuide
//
//  Created by Maulesh Kanani on 11/02/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

#import "PreventBackup.h"

@implementation PreventBackup

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    NSError *error = nil;
    BOOL success = [URL setResourceValue: [NSNumber numberWithBool: YES]
                                  forKey: NSURLIsExcludedFromBackupKey error: &error];
    if(!success){
//        NSLog(@"Error excluding %@ from backup %@", [URL lastPathComponent], error);
    }
//    NSLog(@"prevent backup method called without error");
    return success;
}

@end
