//
//  WebImageOperations.h
//  StoreGuide
//
//  Created by Maulesh Kanani on 11/02/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebImageOperations : NSObject
+ (void)processImageDataWithURLString:(NSString *)urlString andBlock:(void (^)(NSData *imageData))processImage;
@end
