//
//  notificationVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 21/02/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
class notificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    var myRootRef: FIRDatabaseReference!
    @IBOutlet weak var tblnotification: UITableView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var btncross: UIButton!
    var arrnotification = NSMutableArray()
    override func viewDidLoad()
    {
        super.viewDidLoad()

        btncross.layer.cornerRadius = btncross.frame.size.width / 2
        btncross.clipsToBounds = true
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        spinner.startAnimating()
        myRootRef = FIRDatabase.database().reference()
         myRootRef.child(uid!).child("Notification").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    self.spinner.stopAnimating()
                    let actionsheet = UIAlertController(title: "", message: "Notification not found", preferredStyle: UIAlertControllerStyle.alert)
                    let OK = UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: {
                        (alert: UIAlertAction!) -> Void in
                    })
                    actionsheet.addAction(OK)
                    self.present(actionsheet, animated: true, completion: nil)
                }
                else
                {
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                    let allkey : NSArray = data.allKeys as NSArray
                    for i in 0 ..< allkey.count
                    {
                        let key = allkey[i] as! String
                        let arrtemp : NSMutableDictionary = data[key] as! NSMutableDictionary
                        arrtemp.setObject(key, forKey: "allkey" as NSCopying)
                        self.arrnotification.add(arrtemp)
                        
                    }
                    let defaults = UserDefaults.standard
                    defaults.set(self.arrnotification.count, forKey: "count")
                    self.tblnotification.reloadData()
                    self.spinner.stopAnimating()
                }
        })
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        
    }
    @IBAction func btncross(_ sender: Any) {
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    //MARK:- tableview delegate methods---------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return arrnotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:Labelcell = self.tblnotification.dequeueReusableCell(withIdentifier: "Labelcell") as! Labelcell
        
        cell.selectionStyle=UITableViewCellSelectionStyle.none
        let dict = arrnotification[indexPath.row]as! NSDictionary
        cell.lblname.text = dict["notification"]as! String
        cell.btnno.tag = indexPath.row
        cell.btnyes.tag = indexPath.row
        cell.btnno.addTarget(self, action: #selector(notificationVC.btnnoPrassed), for: UIControlEvents.touchUpInside)
        cell.btnyes.addTarget(self, action: #selector(notificationVC.btnyesPrassed), for: UIControlEvents.touchUpInside)

        return cell
    }
    func btnnoPrassed(sender: UIButton)
    {
        spinner.startAnimating()
        self.view.isUserInteractionEnabled = false

        let indexPath : IndexPath = IndexPath(item: sender.tag, section: 0)
        let dicttemp = arrnotification[indexPath.row]as! NSDictionary
        print(dicttemp)
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        myRootRef = FIRDatabase.database().reference()
        
        myRootRef.child("Note").child(dicttemp["key"]as! String).child("accessuid").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    
                    
                }
                else
                {
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                        print(data)
                    let accessuser : NSArray = data["userid"]as! NSArray
                     var arruser = NSMutableArray()
                     for i in 0..<accessuser.count
                     {
                        if accessuser[i]as? String == uid
                        {
                            
                        }
                        else
                        {
                            arruser.add(accessuser[i]as! String)
                        }
                        
                        self.myRootRef.child("Note").child(dicttemp["key"]as! String).child("accessuid").setValue([
                            "userid": arruser])
                        self.myRootRef.child(uid!).child("Notification").child(dicttemp["allkey"]as! String).removeValue()
                    }
                    self.spinner.startAnimating()
                    self.view.isUserInteractionEnabled = false
                    
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                    self.navigationController?.pushViewController(controller, animated: true)
            }
        })
    }
    
    func btnyesPrassed(sender: UIButton)
    {
        spinner.startAnimating()
        self.view.isUserInteractionEnabled = false
        
        let indexPath : IndexPath = IndexPath(item: sender.tag, section: 0)
        let dicttemp = arrnotification[indexPath.row]as! NSDictionary
      
        DBOperation .executeSQL(String(format: "delete from Notes where key='%@'",dicttemp["key"]as! String))
        let defaults = UserDefaults.standard
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child("Note").child(dicttemp["key"]as! String).child("data").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    print("data not found")
                }
                else
                {
                    let arrtemp : NSDictionary  = (snapshot.value as? NSDictionary)!
                    print(arrtemp)
                    let arrlabel : NSArray = arrtemp["Labels"]as! NSArray
                    let arrimages : NSArray = arrtemp["Images"]as! NSArray
                    let arrcheck : NSArray = arrtemp["checklist"]as! NSArray
                    self.addimages(arr: arrimages, noteuid: arrtemp["note_uid"]as! String, key: dicttemp["key"]as! String)
                    self.checklist(arr: arrcheck, noteuid: arrtemp["note_uid"]as! String, key: dicttemp["key"]as! String)
                    let audio : String = arrtemp["audio"]as! String
                    if audio != " "
                    {
                        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                         let storageRef = FIRStorage.storage().reference()
                         let starsRef = storageRef.child("audio/" + audio)
                         
                         let fileURL = documentsDirectoryURL.appendingPathComponent(audio)
                         _ = starsRef.write(toFile: fileURL) { url, error in
                         if let error = error {
                         print(error.localizedDescription)
                         
                         } else
                         {
                         DBOperation.executeSQL(String(format: "Insert into Note_audio('noite_uid','audio_name','key') Values('%@','%@','%@')",(arrtemp["note_uid"]as? String)!,(url?.absoluteString)!,dicttemp["key"]as! String))
                         }
                         }
                    }
                    let Height : Float = Float(arrtemp["cellheight"]as! String)!as Float
                    print(arrtemp)
                    DBOperation.executeSQL(String(format: "Insert into Notes('Title','SubTitle','note_uid','label','key','hexa','cellheight','display') Values('%@','%@','%@','%@','%@','%@','%f','%@')",arrtemp["Title"]as! String,arrtemp["description"]as! String,arrtemp["note_uid"]as! String,arrlabel.componentsJoined(by: ","),dicttemp["key"]as! String,arrtemp["color"]as! String,Height,"true"))
                    
                    self.myRootRef.child(uid!).child("Notification").child(dicttemp["allkey"]as! String).removeValue()
                    
                    self.spinner.stopAnimating()
                    self.view.isUserInteractionEnabled = true
                    
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
                    self.navigationController?.pushViewController(controller, animated: true)
                }
                
                })
        { (error) in
            print(error.localizedDescription)
        }
    }
    
    func checklist(arr : NSArray,noteuid:String , key: String)
    {
        for j in 0 ..< arr.count
        {
            let dict = arr[j]as! NSDictionary
            if dict["checklist"]as! String != ""
            {
                DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",noteuid,dict["checklist"]as! String,dict["flag"]as! String,key))
                
            }
        }
    }
    
    func addimages(arr : NSArray,noteuid:String , key: String)
    {
        var urlString = String()
        for j in 0 ..< arr.count
        {
            if  arr[j]as! String != ""
            {
                let name = arr[j]as! String
                let storageRef = FIRStorage.storage().reference()
                let starsRef = storageRef.child("imags/" + name)
                
                starsRef.data(withMaxSize: 1 * 1024 * 1024) { data, error in
                    if let error = error {
                        
                    }
                    else
                    {
                        let image = UIImage(data: data!)
                        
                        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                        let fileURL = documentsDirectoryURL.appendingPathComponent(arr[j]as! String)
                        urlString = fileURL.path
                        
                        if !FileManager.default.fileExists(atPath: fileURL.path)
                        {
                            if (try? UIImageJPEGRepresentation(image!, 0.1)!.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])) != nil
                            {
                                print("file saved")
                            }
                            else
                            {
                                print("error saving file")
                            }
                        }
                        DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name','key') Values('%@','%@','%@')",noteuid,urlString,key))
                    }
                }
            }
        }
    }
}
