//
//  NotesVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 22/11/16.
//  Copyright © 2016 Maulesh Kanani. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class NotesVC:UIViewController,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UICollectionViewDataSource,UICollectionViewDelegate,customLayoutDelegate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet var tblnotes: UITableView!
    @IBOutlet var height : NSLayoutConstraint!
    @IBOutlet weak var lblnotification: UILabel!
    @IBOutlet weak var imagecollection: UICollectionView!
    var columncount = Int()
    var miniInteriorSpacing = Int()
    var arrnote = NSDictionary()
    var arrItems = NSMutableArray()
    var arrdata : NSMutableArray = []
    var cx = CGFloat(0)
    var tag = Int(0)
    var myRootRef: FIRDatabaseReference!
    var arrheight = NSMutableArray()
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.isUserInteractionEnabled = true
        columncount = 2
        miniInteriorSpacing = 5
        if (!imagecollection.collectionViewLayout .isKind(of: customLayout.self))
        {
            let layout = customLayout()
            layout.delegate = self
            layout.columnCount = CGFloat(columncount)
            imagecollection.collectionViewLayout = layout
            imagecollection .reloadData()
        }
        
        arrdata.removeAllObjects()
        if arrnote.count != 0
        {
            let data : NSArray = DBOperation.selectData(String(format: "select * from Notes"))
            for i in 0..<data.count
            {
                let points : String = (data[i] as! NSDictionary)["label"]as! String
                let dicttemp = data[i] as! NSDictionary
                let fullNameArr = points.components(separatedBy: ",")
                
                for i in 0..<fullNameArr.count
                {
                    if fullNameArr[i] == arrnote["labelname"]as? String
                    {
                        arrdata.add(dicttemp)
                    }
                }
            }
        }
        else
        {
            arrdata = DBOperation.selectData(String(format: "select * from Notes where display='true' order by date DESC"))
        }
        
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        
        
        myRootRef = FIRDatabase.database().reference()
        myRootRef.child(uid!).child("Notification").observeSingleEvent(of: .value, with:
            { (snapshot) in
                let data  = snapshot.value as? NSDictionary
                if data?.count == 0 || data == nil
                {
                    self.lblnotification.text = "0"
                }
                else
                {
                    let data : NSDictionary  = (snapshot.value as? NSDictionary)!
                    let arr : NSArray = data.allKeys as NSArray
                    
                    let counts : Int = defaults.value(forKey: "count") as! Int
                    if counts == arr.count
                    {
                        self.lblnotification.text = "0"
                    }
                    else
                    {
                        if arr.count == 1
                        {
                            let data = arr.count
                            self.lblnotification.text = data.description
                        }else
                        {
                            let data = arr.count - counts
                            self.lblnotification.text = data.description
                        }
                    }
                }
        })
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
       
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func btnrecordaudio(_ sender: Any) {
    
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "recordAudioVC") as! recordAudioVC
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    
    @IBAction func btntakenote(_ sender: Any) {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "WritenoteVC") as! WritenoteVC
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
   
    @IBAction func sidebarPrassed(_ sender: Any) {
        self.mm_drawerController .toggle(MMDrawerSide.left, animated: true, completion: nil)
    }
    
    @IBAction func btncaptureimag(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        
        let CameraRoll = UIAlertAction(title: "Camera Roll", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
            
        })
        
        
        let Library = UIAlertAction(title: "Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
        })
        
        let Camera = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                imagePickerController.allowsEditing = false
                self.present(imagePickerController, animated: false, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Error!!", message: "Device Have No Camera", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(CameraRoll)
        optionMenu.addAction(Library)
        optionMenu.addAction(Camera)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    
    @IBAction func btnlistprassed(_ sender: Any)
    {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "listVC") as! listVC
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    @IBAction func btnnotification(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "notificationVC") as! notificationVC
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    //Mark:- action sheet delegate methods----------------------------------------------------------
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let imagePath = formatter.string(from: now)
        let name = imagePath + "" + ".jpg"
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent(name)
        let urlString: String = fileURL.path
        if !FileManager.default.fileExists(atPath: fileURL.path)
        {
            if (try? UIImageJPEGRepresentation(image, 0.1)!.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])) != nil
            {
                print("file saved")
            }
            else
            {
                print("error saving file")
            }
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
    }
    
    //MARK:- collection view methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrdata.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = imagecollection.dequeueReusableCell(withReuseIdentifier: "Collectimage",for: indexPath) as! Collectimage
        
        if arrdata.count != 0
        {

            let dicttemp = arrdata[indexPath.row] as! NSDictionary
            cell.title.text = dicttemp["Title"]as? String
            cell.lbldicription.text = dicttemp["SubTitle"]as? String
            cell.contentView.backgroundColor = hexStringToUIColor(hex: (dicttemp["hexa"]as? String)!)
            let arrcheck : NSArray = DBOperation.selectData(String(format: "select * from checklist where noteuid='%@'",dicttemp["note_uid"]as! String))
            var Ypoint : CGFloat = 5.0
            var Ypoint1 : CGFloat = 2.0
            
            if arrcheck.count != 0
            {
                cell.lbldicription.isHidden = true
                cell.viewcheck.isHidden = false
                let maxSize : CGSize = CGSize(width: self.view.frame.size.width - 30, height: 9999)
                
                for i in 0 ..< arrcheck.count
                {
                    let strItem = arrcheck[i] as! NSDictionary
                    let btnlabel = UILabel()
                    let titlesize : CGSize =  NSUtil .dynamicSize(fromText: strItem["data"]as! String, with: UIFont.systemFont(ofSize: 10.0), withMaxSize: maxSize)
                     btnlabel .frame = CGRect(x: 25.0 , y: Ypoint1, width: titlesize.width + 20 , height: titlesize.height + 10)
                    btnlabel.text = strItem["data"]as? String
                    btnlabel.font = UIFont.systemFont(ofSize: 10.0)
                    let btncheck = UIButton()
                    if strItem["flag"]as! String == "true"
                    {
                        btncheck .frame = CGRect(x: 5.0, y: Ypoint, width: 15 , height: 15)
                        btncheck.setImage(UIImage(named: "checktick.png"), for: UIControlState.normal)
                    }
                    else
                    {
                        btncheck .frame = CGRect(x: 5.0, y: Ypoint, width: 15 , height: 15)
                        btncheck.setImage(UIImage(named: "checkbox.png"), for: UIControlState.normal)
                    }
                   Ypoint = Ypoint + btncheck.frame.height
                   Ypoint1 = Ypoint1 + btncheck.frame.height

            
                    cell.viewcheck.addSubview(btncheck)
                    cell.viewcheck .addSubview(btnlabel)
                }

            }
            
            let arraddimages : NSArray = DBOperation.selectData(String(format: "select * from Note_image where noite_uid='%@'",dicttemp["note_uid"]as! String))
            if arraddimages.count != 0
            {
                cell.constantheight.isActive = true
                
                let subViews = cell.scroll.subviews
                for subview in subViews
                {
                    subview.removeFromSuperview()
                }

                if arraddimages.count == 1
                {
                    let dict = arraddimages[0] as! NSDictionary
                    let imageview = UIImageView()
                    imageview.frame = CGRect(x: cx, y: 0, width: cell.scroll.frame.width, height: cell.scroll.frame.height)
                    NSUtil .getImageFor(imageview, fromURL: dict["image_name"] as? String, alterText: "")
                    cell.scroll.addSubview(imageview)

                }
                else if arraddimages.count == 2
                {
                    for i in 0 ..< arraddimages.count
                    {
                        let dict = arraddimages[i] as! NSDictionary
                        let imageview = UIImageView()
                        imageview.frame = CGRect(x: cx, y: 0, width: cell.scroll.frame.width / 2, height: cell.scroll.frame.width / 2)
                        NSUtil .getImageFor(imageview, fromURL: dict["image_name"] as? String, alterText: "")
                        cell.scroll.addSubview(imageview)
                        cx += imageview.frame.size.width + 2
                        cell.scroll.contentSize.width = cx
                        cell.scroll.contentSize.height = 40
                    }
                }
                else
                {
                    for i in 0 ..< arraddimages.count
                    {
                        let dict = arraddimages[i] as! NSDictionary
                        let imageview = UIImageView()
                        imageview.frame = CGRect(x: cx, y: 0, width: 40, height: 40)
                        NSUtil .getImageFor(imageview, fromURL: dict["image_name"] as? String, alterText: "")
                        cell.scroll.addSubview(imageview)
                        cx += imageview.frame.size.width + 2
                        cell.scroll.contentSize.width = cx
                        cell.scroll.contentSize.height = 40
                    }

                }
                cx = CGFloat(0)
            }
            else
            {
                cell.constantheight.isActive = false
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        let dicttemp = arrdata[indexPath.row] as! NSDictionary
     
        let arrcheck : NSArray = DBOperation.selectData(String(format: "select * from checklist where noteuid='%@'",dicttemp["note_uid"]as! String))
        if arrcheck.count != 0
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "listVC") as! listVC
            viewcontroller.dictnote = arrdata[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(viewcontroller, animated: true)

        }
        else
        {
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "WritenoteVC") as! WritenoteVC
            viewcontroller.dictnote = arrdata[indexPath.row] as! NSDictionary
            self.navigationController?.pushViewController(viewcontroller, animated: true)

        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 5.0;
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let cellsAcross: CGFloat = 2.0
        let spaceBetweenCells: CGFloat = 5.0
      let width : CGFloat = (imagecollection.bounds.width  - (cellsAcross + 1) * spaceBetweenCells) / cellsAcross
      
        let dicttemp = arrdata[indexPath.row] as! NSDictionary
        let Height : Float = Float(dicttemp["cellheight"]as! String)!as Float
        return CGSize(width: width, height: CGFloat(Height))
    }
}
