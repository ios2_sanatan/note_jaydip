//
//  recordAudioVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 25/01/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit
import AVFoundation

class recordAudioVC: UIViewController,AVAudioRecorderDelegate
 {
    
    
    @IBOutlet weak var btnaudio: UIButton!
    @IBOutlet weak var lblrecord: UILabel!
    
    var recordingSession : AVAudioSession!
    var audioRecorder    :AVAudioRecorder!
    var settings         = [String : Int]()
    var pathString  : String = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnaudio.layer.cornerRadius = btnaudio.frame.size.width / 2
        recordingSession = AVAudioSession.sharedInstance()
        do {
            try recordingSession.setCategory(AVAudioSessionCategoryPlayAndRecord)
            try recordingSession.setActive(true)
            recordingSession.requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        print("Allow")
                    } else {
                        print("Dont Allow")
                    }
                }
            }
        } catch {
            print("failed to record!")
        }
        
        // Audio Settings
        
        settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
           }

    func directoryURL() -> NSURL? {
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let imagePath = formatter.string(from: now)
        let name = "audio" + imagePath + ".m4a"
        let fileManager = FileManager.default
        let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = urls[0] as NSURL
        let soundURL = documentDirectory.appendingPathComponent(name)
        pathString = (soundURL?.absoluteString)!
        return soundURL as NSURL?
    }
    func startRecording()
    {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            audioRecorder = try AVAudioRecorder(url: self.directoryURL()! as URL,
                                                settings: settings)
            audioRecorder.delegate = self
            audioRecorder.prepareToRecord()
        } catch {
            finishRecording(success: false)
        }
        do {
            try audioSession.setActive(true)
            audioRecorder.record()
        } catch {
   
        }
    }
    func finishRecording(success: Bool) {
        audioRecorder.stop()
        if success {
            print(success)
        } else {
            audioRecorder = nil
            print("Somthing Wrong.")
        }
    }
    //MARK:- button toucjh up methods----------------------------------
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        if !flag
        {
            finishRecording(success: false)
        }
    }
    
    @IBAction func btnaudioprassed(_ sender: Any)
    {
        if audioRecorder == nil {
            
            lblrecord.isHidden = false
            self.startRecording()
            
        } else {
           lblrecord.text = "recording finish........."
            self.finishRecording(success: true)
        }
    }
    
    @IBAction func btncrossprassed(_ sender: Any)
    {
        if audioRecorder != nil {
        
            self.finishRecording(success: true)
            pathString = ""
        }
    }
    
    @IBAction func btnbackprassed(_ sender: Any)
    {
        if audioRecorder != nil {
            lblrecord.text = ""
            self.finishRecording(success: true)
        }
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "WritenoteVC") as! WritenoteVC
        viewcontroller.urlString = pathString
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
}
