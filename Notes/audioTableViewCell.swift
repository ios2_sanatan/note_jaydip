//
//  audioTableViewCell.swift
//  Notes
//
//  Created by Maulesh Kanani on 30/01/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit

class audioTableViewCell: UITableViewCell {

    @IBOutlet weak var btnplay: UIButton!
    @IBOutlet weak var lbltime: UILabel!
    @IBOutlet weak var progress: UIProgressView!
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var btntick: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
