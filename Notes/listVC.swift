//
//  listVC.swift
//  Notes
//
//  Created by Maulesh Kanani on 08/02/17.
//  Copyright © 2017 Maulesh Kanani. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices
import FirebaseDatabase
import Firebase

class listVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextViewDelegate,selectlabelnamedelegate,AVAudioPlayerDelegate,UITableViewDataSource,UITableViewDelegate,selectdate,UICollectionViewDelegateFlowLayout
{
    @IBOutlet var viewcolor: UIView!
    @IBOutlet weak var viewmain: UIView!
    @IBOutlet weak var btnsetting: UIButton!
    @IBOutlet weak var actionsheet: UIView!
    @IBOutlet weak var scroll: UIScrollView!
    @IBOutlet weak var tbllist: UITableView!
    @IBOutlet weak var height: NSLayoutConstraint!
    @IBOutlet weak var height1: NSLayoutConstraint!
    @IBOutlet weak var collimg: UICollectionView!
    @IBOutlet weak var lbldate_time: UILabel!
    var cellheight = Float()
    var color : String = "FFFFFF"
    var myRootRef: FIRDatabaseReference!
    var uid = String()
    var cx = CGFloat(10)
    var tag = Int(0)
    
    var arrItems = NSMutableArray()
    var fileid = String()
    var urlString = String()
    var shareid = String()
    var arrimages = NSMutableArray()
    var addimages = NSMutableArray()
    var dictnote = NSDictionary()
    var arraudio = NSMutableArray()
    var arrcheck = NSMutableArray()
    
    @IBOutlet var constBottom : NSLayoutConstraint!
    @IBOutlet weak var txttitle: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        let defaults = UserDefaults.standard
        uid = (defaults.value(forKey:"uid")as? String)!
        if dictnote.count != 0
        {
            txttitle.text = dictnote["Title"]as? String
            let points : String = dictnote["label"]as! String
            let fullNameArr = points.components(separatedBy: ",")
            color = dictnote["hexa"]as! String
            viewcolor.backgroundColor = hexStringToUIColor(hex: color)
            for i in 0..<fullNameArr.count
            {
                if fullNameArr[i] != ""
                {
                    arrItems.add(fullNameArr[i])
                }
                
            }
            tag = 1
            arraudio = DBOperation.selectData(String(format: "select * from checklist where noteuid='%@'",dictnote["note_uid"]as! String))
            let arrimg : NSArray = DBOperation.selectData(String(format: "select * from Note_image where noite_uid='%@'",dictnote["note_uid"]as! String))
            
            for i in 0..<arrimg.count
            {
                let temp = (arrimg[i] as? NSDictionary)?.object(forKey: "image_name")as? String
                addimages.add(temp!)
            }
            collimg.reloadData()
            self.setframe()
        }
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd:MM:yyyy hh:mm"
        let keystring = formatter.string(from: now)
        lbldate_time.text = keystring.appending(":00")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
       
    }
    
    func calculateheight () ->Void
    {
        if addimages.count != 0
        {
            cellheight = 85.0
        }
       
        let number = arraudio.count
        let height1 : CGFloat =  20 * CGFloat(number)
        cellheight = cellheight + 17.0 + Float(height1)
        if cellheight >= 150.0
        {
            cellheight = 150.0
        }
    }

    //MARK:- button touchup methods-----------------------------------------
    
    func labelname(arr : NSMutableArray)
    {
        arrItems = arr
        if arrItems.count == 0
        {
            scroll.isHidden = true
        }else
        {
            scroll.isHidden = false
        }
        
        self.setframe()
    }
    
    func getdate(arr: String)
    {
       // lbldate_time.text = arr
    }
    
    
    @IBAction func btnaddlistprasse(_ sender: Any)
    {
        if arraudio.count != 0
        {
            arraudio.add(["flag":"false","data":""])
        }
        else
        {
            arraudio.add(" ")
        }
        tbllist.reloadData()
    }
    
    @IBAction func btnshareprassed(_ sender: Any) {
        
        savedata()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "searchuserVC") as! searchuserVC
        vc.keyid = shareid
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    
    @IBAction func btnbackprassed(_ sender: Any)
    {
        savedata()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let  controller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func savedata()-> Void
    {
        self.calculateheight()
        for i in 0..<addimages.count
        {
            let url = NSURL(fileURLWithPath: addimages[i]as! String)
            let soundFileURL = url.lastPathComponent
            arrimages.add(soundFileURL!)
        }
        
        let defaults = UserDefaults.standard
        let uid = defaults.value(forKey:"uid")as? String
        
        let autono : NSArray = DBOperation.selectData(String(format:"select * from Notes"))
        let Randomnumber : Int = Int(arc4random_uniform(UInt32(10000)))
        let number :Int = Int (arc4random() % 16)
        let abc : Int = autono.count + 1;
        let final : String = "note" + abc.description + Randomnumber.description + number.description
        
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let keystring = formatter.string(from: now)
        let key = keystring + autono.count.description
        
        var name = String()
        
        if urlString == ""
        {
            name = " "
        }
        else
        {
            let url = NSURL(fileURLWithPath: urlString)
            let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
            let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
            name  =  url.lastPathComponent!
            let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
            let localFile = URL(string: documentsPath)!
            let storageRef = FIRStorage.storage().reference()
            let mountainsRef = storageRef.child("audio/"+name)
            let metadata = FIRStorageMetadata()
            metadata.contentType = "audio/mpeg"
            mountainsRef.put(data as Data, metadata: metadata)
            mountainsRef.putFile(localFile, metadata: metadata)
            
        }
        if txttitle.text != ""
        {
            myRootRef = FIRDatabase.database().reference()
            if dictnote.count == 0
            {
                for i in 0 ..< arraudio.count
                {
                    let indexPath : IndexPath = IndexPath(item: i, section: 0)
                    let cell:audioTableViewCell = tbllist.cellForRow(at: indexPath) as! audioTableViewCell!
                    if cell.txtname.text == ""
                    {
                    }
                    else
                    {
                        var dict = NSMutableDictionary()
                        if cell.btntick.isSelected == true
                        {
                            DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",final,cell.txtname.text!,"true",key))
                            dict = ["checklist":cell.txtname.text!,"flag":"true"]
                        }
                        else
                        {
                            DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",final,cell.txtname.text!,"false",key))
                            dict = ["checklist":cell.txtname.text!,"flag":"false"]
                        }
                        arrcheck.add(dict)
                    }
                }
                
                
                var addimages = NSMutableArray()
                var addItems = NSMutableArray()
                var addcheck = NSMutableArray()
                
                if arrimages.count == 0
                {
                    addimages = [""]
                }
                else
                {
                    addimages = arrimages
                }
                
                if arrItems.count == 0
                {
                    addItems = [""]
                }
                else
                {
                    addItems = arrItems
                }
                
                if arrcheck.count == 0
                {
                    addcheck = [""]
                }
                else
                {
                    addcheck = arrcheck
                }
                shareid = key
                self.myRootRef.child("Note").child(key).child("data").setValue([
                    "Title": txttitle.text!,
                    "description":" ",
                    "note_uid":final,
                    "color" : color,
                    "display": "true",
                    "cellheight":cellheight.description,
                    "audio" : name.description,
                    "time":lbldate_time.text!,
                    "Images" : addimages,"Labels":addItems,"checklist" : addcheck])
                
                 let arruid : NSArray = [uid]
                self.myRootRef.child("Note").child(key).child("accessuid").setValue([
                    "userid": arruid])

                DBOperation.executeSQL(String(format: "Insert into Notes('Title','SubTitle','note_uid','label','key','hexa','cellheight','display') Values('%@','%@','%@','%@','%@','%@','%f','%@')",txttitle.text!," ",final,arrItems.componentsJoined(by: ","),key,color,cellheight,"true"))
                self.saveimage(noteid :final)
                
                
            }else{
                
                DBOperation .executeSQL(String(format: "delete from checklist where noteuid='%@'",dictnote["note_uid"] as! String))
                for i in 0 ..< arraudio.count
                {
                    let indexPath : IndexPath = IndexPath(item: i, section: 0)
                    let cell:audioTableViewCell = tbllist.cellForRow(at: indexPath) as! audioTableViewCell!
                    if cell.txtname.text == ""
                    {
                    }
                    else
                    {
                        var dict = NSMutableDictionary()
                        if cell.btntick.isSelected == true
                        {
                            DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",dictnote["note_uid"] as! String,cell.txtname.text!,"true",key))
                            dict = ["checklist":cell.txtname.text!,"flag":"true"]
                        }
                        else
                        {
                            DBOperation.executeSQL(String(format: "Insert into checklist('noteuid','data','flag','key') Values('%@','%@','%@','%@')",dictnote["note_uid"] as! String,cell.txtname.text!,"false",key))
                            dict = ["checklist":cell.txtname.text!,"flag":"false"]
                        }
                        arrcheck.add(dict)
                    }
                }
                
                var addimages = NSMutableArray()
                var addItems = NSMutableArray()
                var addcheck = NSMutableArray()
                
                if arrimages.count == 0
                {
                    addimages = [""]
                }
                else
                {
                    addimages = arrimages
                }
                
                if arrItems.count == 0
                {
                    addItems = [""]
                }
                else
                {
                    addItems = arrItems
                }
                
                if arrcheck.count == 0
                {
                    addcheck = [""]
                }
                else
                {
                    addcheck = arrcheck
                }
                shareid = dictnote["key"]as! String
                self.myRootRef.child("Note").child(dictnote["key"]as! String).child("data").setValue([
                    "Title": txttitle.text!,
                    "description":" ",
                    "note_uid":dictnote["note_uid"] as! String,
                    "color" : color,
                    "display": "true",
                    "cellheight":cellheight.description,
                    "audio" : name.description,
                    "time":lbldate_time.text!,
                    "Images" : addimages,"Labels":addItems,"checklist" : addcheck])
                
                let arruid : NSArray = [uid]
                self.myRootRef.child("Note").child(dictnote["key"]as! String).child("accessuid").setValue([
                    "userid": arruid])
                self.saveimage(noteid :dictnote["note_uid"] as! String)
                DBOperation.executeSQL(String(format: "update Notes set Title='%@',SubTitle='%@',label='%@',hexa='%@',cellheight='%f' where note_uid='%@'",txttitle.text!," ",arrItems.componentsJoined(by: ","),color,cellheight,dictnote["note_uid"] as! String))
                
            }
            
        }

    }
    @IBAction func btnsettimeprassed(_ sender: Any) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "Date_timeVC") as! Date_timeVC
        vc.delegate = self
        vc.dictdata = dictnote
        self.view.addSubview(vc.view)
        self.addChildViewController(vc)
        vc.didMove(toParentViewController: self)
    }
    
    @IBAction func btnsettingprassed(_ sender: Any)
    {
        if btnsetting.isSelected == true
        {
            btnsetting.isSelected = false
            actionsheet.isHidden = true
        }else
        {
            btnsetting.isSelected = true
            actionsheet.isHidden = false
        }

    }
    
    @IBAction func btnimgprassed(_ sender: Any)
    {
        let optionMenu = UIAlertController(title: nil, message: "", preferredStyle: .actionSheet)
        
        
        let CameraRoll = UIAlertAction(title: "Camera Roll", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.savedPhotosAlbum
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
            
        })
        
        
        let Library = UIAlertAction(title: "Library", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePickerController.allowsEditing = true
            self.present(imagePickerController, animated: false, completion:nil)
        })
        
        let Camera = UIAlertAction(title: "Camera", style: .default, handler: {
            (alert: UIAlertAction!) -> Void in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)
            {
                let imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = UIImagePickerControllerSourceType.camera;
                imagePickerController.allowsEditing = false
                self.present(imagePickerController, animated: false, completion: nil)
            }
            else
            {
                let alert = UIAlertController(title: "Error!!", message: "Device Have No Camera", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        optionMenu.addAction(CameraRoll)
        optionMenu.addAction(Library)
        optionMenu.addAction(Camera)
        optionMenu.addAction(cancelAction)
        self.present(optionMenu, animated: true, completion: nil)
    }
    func saveimage(noteid : String)
    {
        if dictnote.count != 0
        {
            DBOperation.executeSQL(String(format: "delete from Note_image where noite_uid='%@'",noteid))
            for i in 0..<addimages.count
            {
                let url = NSURL(fileURLWithPath: addimages[i]as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
                let name  =  url.lastPathComponent!
                let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
                let localFile = URL(string: documentsPath)!
                let storageRef = FIRStorage.storage().reference()
                let mountainsRef = storageRef.child("imags/"+name)
                
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                mountainsRef.put(data as Data, metadata: metadata)
                mountainsRef.putFile(localFile, metadata: metadata)
                
                DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name') Values('%@','%@')",noteid,(addimages[i]as? String)!))
            }
            
        }else{
            
            for i in 0..<addimages.count
            {
                let url = NSURL(fileURLWithPath: addimages[i]as! String)
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
                let soundFileURL = documentsPath.appending("/\(url.lastPathComponent!)")
                let name  =  url.lastPathComponent!
                let data: NSData = NSData.dataWithContentsOfMappedFile(soundFileURL) as! NSData
                let localFile = URL(string: documentsPath)!
                let storageRef = FIRStorage.storage().reference()
                let mountainsRef = storageRef.child("imags/"+name)
                
                let metadata = FIRStorageMetadata()
                metadata.contentType = "image/jpeg"
                mountainsRef.put(data as Data, metadata: metadata)
                mountainsRef.putFile(localFile, metadata: metadata)
                
                DBOperation.executeSQL(String(format: "Insert into Note_image('noite_uid','image_name') Values('%@','%@')",noteid,(addimages[i]as? String)!))
            }
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor
    {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0))
    }
    
    @IBAction func btncancel(_ sender: UIButton)
    {
        if sender.tag == 1
        {
            actionsheet.isHidden = true
            if self.dictnote.count != 0
            {
                let defaults = UserDefaults.standard
                let uid = defaults.value(forKey:"uid")as? String
                let ref = FIRDatabase.database().reference()
                
                let arraddimages : NSArray = DBOperation.selectData(String(format: "select * from Note_image where noite_uid='%@'",dictnote["note_uid"]as! String))
                for i in 0..<arraddimages.count
                {
                    let dicttemp : NSDictionary = arraddimages[i]as! NSDictionary
                    let url = NSURL(fileURLWithPath: dicttemp["image_name"]as! String)
                    let soundFileURL = url.lastPathComponent
                    print(soundFileURL!)
                    
                    let storageRef = FIRStorage.storage().reference()
                    let mountainsRef = storageRef.child("imags/" + soundFileURL!)
                    mountainsRef.delete { error in
                        if error != nil {
                            print("error")
                        } else {
                            print("deleted")
                        }
                    }
                }
                
                ref.child(uid!).child("Note").child(self.dictnote["key"]as! String).removeValue()
                DBOperation.executeSQL(String(format: "delete from Notes where note_uid = '%@'",self.dictnote["note_uid"] as! String))
                DBOperation.executeSQL(String(format: "delete from Note_image where noite_uid = '%@'",self.dictnote["note_uid"] as! String))
            }
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewcontroller = mainStoryboard.instantiateViewController(withIdentifier: "NotesVC") as! NotesVC
            self.navigationController?.pushViewController(viewcontroller, animated: true)
            
        }
        if sender.tag == 2
        {
            actionsheet.isHidden = true
            if self.txttitle.text != ""
            {
                tag = 6
                let now = NSDate()
                let formatter = DateFormatter()
                formatter.dateFormat = "ddMMyyHHmmss"
                formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
                let datestring = formatter.string(from: now as Date)
                let pdfname = self.txttitle.text! + datestring + ".pdf"
            }
        }
        
        if sender.tag == 3
        {
            actionsheet.isHidden = true
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let  controller = mainStoryboard.instantiateViewController(withIdentifier: "LabelVC") as! LabelVC
            controller.note_uid = self.uid
            controller.arrayItem = self.arrItems
            controller.delegate = self
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    @IBAction func btncolorprassed(_ sender: UIButton)
    {
        actionsheet.isHidden = true
        if sender.tag == 1
        {
            color = "FFCBA4"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FFCBA4")
        }
        else if sender.tag == 2
        {
            color = "36DDB6"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "36DDB6")
            
        }
        else if sender.tag == 3
        {
            color = "FF0060"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FF0060")
            
        }
        else if sender.tag == 4
        {
            color = "FF9A0F"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FF9A0F")
            
        }
        else if sender.tag == 5
        {
            color = "7FE0FF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "7FE0FF")
            
        }
        else if sender.tag == 6
        {
            color = "C387FF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "C387FF")
            
        }
        else if sender.tag == 7
        {
            color = "FFFFFF"
            viewcolor.backgroundColor = hexStringToUIColor(hex: "FFFFFF")
            
        }

        
    }

    //MARK:- imagepicker method------------------------------
    
    func imagePickerController(_ picker: UIImagePickerController!, didFinishPickingImage image: UIImage!, editingInfo: NSDictionary!)
    {
        self.dismiss(animated: true, completion: { () -> Void in
            
        })
        let autono : NSArray = DBOperation.selectData(String(format:"select * from Notes"))
        let now = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmss"
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        let imagePath = formatter.string(from: now)
        let name = imagePath + autono.count.description + ".jpg"
        
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent(name)
        let urlString: String = fileURL.path
        
        if !FileManager.default.fileExists(atPath: fileURL.path)
        {
            if (try? UIImageJPEGRepresentation(image, 0.1)!.write(to: URL(fileURLWithPath: fileURL.path), options: [.atomic])) != nil
            {
                print("file saved")
            }
            else
            {
                print("error saving file")
            }
        }
        addimages.add(urlString)
        collimg.reloadData()
    }
    
    //MARK:- collection view methods-----------------------------
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
         let height = collimg.frame.size.width / 3.0
        if addimages.count == 1 || addimages.count == 2 || addimages.count == 3
        {
            let height2 : CGFloat =  height * CGFloat(1)
            height1.constant = height2
            return addimages.count
        }
        else{
            
            var number = Int()
            for i in 0..<addimages.count
            {
                print(i)
                print(i%2)
                if i%3 == 0
                {
                    number = number + 1
                }
            }
            let height2 : CGFloat =  height * CGFloat(number)
            height1.constant = height2
            return addimages.count
        }
        return 0

    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collimg.dequeueReusableCell(withReuseIdentifier: "Collectimage",for: indexPath) as! Collectimage
        cell.backgroundColor = UIColor.black
        if addimages.count != 0
        {
            let dicttemp = addimages[indexPath.row] as! String
            if dicttemp != ""
            {
                NSUtil.getImageFor(cell.imgnote, fromURL: dicttemp, alterText: nil)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        
        let cellsAcross: CGFloat = 3
        let spaceBetweenCells: CGFloat = 1
        let width : CGFloat = (collimg.bounds.width  - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross
        return CGSize.init(width: width, height: collimg.frame.size.width / 3.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1.0;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat
    {
        return 1.0;
    }
    //MARK:- tableview delegate methods-----------------------------
    
    func btnplayprassed(sender: UIButton)
    {
        let indexPath : IndexPath = IndexPath(item: sender.tag, section: 0)
        let cell: audioTableViewCell = self.tbllist.cellForRow(at: indexPath) as! audioTableViewCell
        if cell.btntick .isSelected == true
        {
            cell.btntick .isSelected = false
        }
        else
        {
            cell.btntick .isSelected = true
        }
    }
    
    //MARK:- tableview delegate methods------------------------------------------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        let number = arraudio.count
        let height1 : CGFloat =  tbllist.rowHeight * CGFloat(number)
        height.constant = height1
        return arraudio.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        let cell:audioTableViewCell = self.tbllist.dequeueReusableCell(withIdentifier: "audioTableViewCell") as! audioTableViewCell
        if tag == 1
        {
            let dict = arraudio[indexPath.row]as! NSDictionary
            cell.txtname.text = dict["data"]as? String
            if dict["flag"]as! String == "true"
            {
                cell.btntick.isSelected = true
            }
            else
            {
                    cell.btntick.isSelected = false
            }
            cell.btntick.tag = indexPath.row
            cell.btntick.addTarget(self, action: #selector(listVC.btnplayprassed), for:UIControlEvents.touchUpInside)
        }
        else
        {
            cell.btntick.tag = indexPath.row
            cell.btntick.addTarget(self, action: #selector(listVC.btnplayprassed), for:UIControlEvents.touchUpInside)
        }
       
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell: audioTableViewCell = self.tbllist.cellForRow(at: indexPath) as! audioTableViewCell
        if cell.btntick .isSelected == true
        {
            cell.btntick .isSelected = false
            
        }else{
            cell.btntick .isSelected = true
        }
    }
    
    //MARK:- userdefine mehods----------------------------------------
    func setframe() -> Void
    {
        let subViews = scroll.subviews
        for subview in subViews
        {
            subview.removeFromSuperview()
        }

        var Xpoint : CGFloat = 10.0
        var Ypoint : CGFloat = 10.0
        
        let maxSize : CGSize = CGSize(width: self.view.frame.size.width - 30, height: 9999)
        
        for i in 0 ..< arrItems.count
        {
            let strItem = arrItems[i] as? String
            let btnlabel = UILabel()
            let titlesize : CGSize =  NSUtil .dynamicSize(fromText: strItem, with: UIFont.systemFont(ofSize: 12.0), withMaxSize: maxSize)
            if (Xpoint + titlesize.width + 15 < self.view.frame.size.width-20)
            {
                btnlabel .frame = CGRect(x: Xpoint, y: Ypoint, width: titlesize.width + 20 , height: titlesize.height + 10)
                Xpoint = Xpoint + titlesize.width + 25
            }
            else
            {
                Xpoint = 10;
                Ypoint = Ypoint+titlesize.height+15;
                btnlabel .frame = CGRect(x: Xpoint, y: Ypoint, width: titlesize.width + 20 , height: titlesize.height+10)
                Xpoint = Xpoint + titlesize.width + 25
            }
            btnlabel.text = strItem
            btnlabel .textColor = UIColor .black
            scroll .addSubview(btnlabel)
        }
    }
}
